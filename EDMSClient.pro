QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    applicationstorage.cpp \
    coreclass.cpp \
    createnewdocumentwindow.cpp \
    datetime/datetime.cpp \
    documentcardwindow.cpp \
    documentsignaturecardwindow.cpp \
    enterwindow.cpp \
    jsonParse/jsonparse.cpp \
    main.cpp \
    mainwindow.cpp \
    mydocumentssignaturewindow.cpp \
    mydocumentswindow.cpp \
    notificationwindow.cpp \
    qrcodenotificationwindow.cpp \
    settingswindow.cpp \
    tcpclient.cpp

HEADERS += \
    applicationstorage.h \
    coreclass.h \
    createnewdocumentwindow.h \
    datetime/datetime.h \
    documentcardwindow.h \
    documentsignaturecardwindow.h \
    enterwindow.h \
    jsonParse/jsonparse.h \
    mainwindow.h \
    mydocumentssignaturewindow.h \
    mydocumentswindow.h \
    notificationwindow.h \
    qrcodenotificationwindow.h \
    settingswindow.h \
    tcpclient.h

FORMS += \
    createnewdocumentwindow.ui \
    documentcardwindow.ui \
    documentsignaturecardwindow.ui \
    enterwindow.ui \
    mainwindow.ui \
    mydocumentssignaturewindow.ui \
    mydocumentswindow.ui \
    notificationwindow.ui \
    qrcodenotificationwindow.ui \
    settingswindow.ui

#QZXing
include(QZXing/QZXing.pri)
CONFIG += qzxing_qml
#

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

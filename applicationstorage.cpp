#include "applicationstorage.h"

ApplicationStorage::ApplicationStorage(QObject *parent) : QObject(parent)
{
    m_userID = -1;
    m_userName.clear();
    m_userSurname.clear();
    m_userPostName.clear();
}

void ApplicationStorage::setUserToken(const QString &userToken)
{
    m_userToken = userToken;
}

QString ApplicationStorage::getUserToken()
{
    return m_userToken;
}

void ApplicationStorage::setUserID(const int &userID)
{
    m_userID = userID;
}

int ApplicationStorage::getUserID()
{
    return m_userID;
}

void ApplicationStorage::setUserName(const QString &userName)
{
    m_userName = userName;
}

QString ApplicationStorage::getUserName()
{
    return m_userName;
}

void ApplicationStorage::setUserSurname(const QString &userSurname)
{
    m_userSurname = userSurname;
}

QString ApplicationStorage::getUserSurname()
{
    return m_userSurname;
}

void ApplicationStorage::setUserPostName(const QString &userPostName)
{
    m_userPostName = userPostName;
}

QString ApplicationStorage::getUserPostName()
{
    return m_userPostName;
}


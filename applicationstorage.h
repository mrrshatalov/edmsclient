#ifndef APPLICATIONSTORAGE_H
#define APPLICATIONSTORAGE_H

#include <QObject>

class ApplicationStorage : public QObject
{
    Q_OBJECT
public:
    explicit ApplicationStorage(QObject *parent = nullptr);

    void setUserToken(const QString &userToken);
    QString getUserToken();

    void setUserPassword(const QString &userPassword);
    QString getUserPassword();

    void setUserID(const int &userID);
    int getUserID();

    void setUserName(const QString &userName);
    QString getUserName();

    void setUserSurname(const QString &userSurname);
    QString getUserSurname();

    void setUserPostName(const QString &userPostName);
    QString getUserPostName();


private:
    QString m_userToken;

    int m_userID;
    QString m_userName;
    QString m_userSurname;
    QString m_userPostName;

};

#endif // APPLICATIONSTORAGE_H

#include "coreclass.h"

CoreClass* CoreClass::m_instance = 0;

CoreClass::CoreClass()
{
    m_tcpClient = new TCPClient();
    m_appStorage = new ApplicationStorage();
}

CoreClass::CoreClass(const CoreClass &) {}

TCPClient *CoreClass::tcpClient() const
{
    return m_tcpClient;
}

ApplicationStorage *CoreClass::appStorage() const
{
    return m_appStorage;
}

CoreClass::~CoreClass()
{
    delete m_tcpClient;
    delete m_appStorage;
}

CoreClass& CoreClass::operator =(const CoreClass &)
{
    return *this;
}

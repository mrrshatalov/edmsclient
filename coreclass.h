#ifndef CORECLASS_H
#define CORECLASS_H

#include <QObject>

#include "tcpclient.h"
#include "applicationstorage.h"

class CoreClass : public QObject
{
    Q_OBJECT
public:
    static CoreClass* instance()
    {
        if (!m_instance)
            m_instance = new CoreClass;

        return m_instance;
    }

    TCPClient *tcpClient() const;
    ApplicationStorage *appStorage() const;

private:
    TCPClient *m_tcpClient;
    ApplicationStorage *m_appStorage;

    static CoreClass* m_instance;
    CoreClass();
    ~CoreClass();

    CoreClass(const CoreClass &);
    CoreClass& operator=(const CoreClass &);
};

#endif // CORECLASS_H

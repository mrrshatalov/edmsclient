#include "createnewdocumentwindow.h"
#include "ui_createnewdocumentwindow.h"

#define m_tcpClient CoreClass::instance()->tcpClient()
#define m_appStorage CoreClass::instance()->appStorage()

CreateNewDocumentWindow::CreateNewDocumentWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateNewDocumentWindow)
{
    ui->setupUi(this);

    connect(m_tcpClient, SIGNAL(getUserListFinished(QString)),
            this, SLOT(onGetUserListFinished(QString)));
    connect(m_tcpClient, SIGNAL(createNewDocumentFinished(QString)),
            this, SLOT(onCreateNewDocumentFinished(QString)));

    setEnabledFunctional(false);
    //Получить список всех сотрудников (кроме себя)
    m_tcpClient->getUserList(m_appStorage->getUserToken());
}

CreateNewDocumentWindow::~CreateNewDocumentWindow()
{
    delete ui;
}

void CreateNewDocumentWindow::closeEvent(QCloseEvent *event)
{
    emit closeWindow();
}

void CreateNewDocumentWindow::setEnabledFunctional(const bool &isEnabled)
{
    ui->documentNameLine->setEnabled(isEnabled);
    ui->documentShortDescriptionEdit->setEnabled(isEnabled);
    ui->userList->setEnabled(isEnabled);
    ui->browseButton->setEnabled(isEnabled);
    ui->createDocumentButton->setEnabled(isEnabled);
}

void CreateNewDocumentWindow::onGetUserListFinished(const QString &jsonData)
{
    if (jsonData.indexOf("getUserList", Qt::CaseInsensitive) != -1) {

        QString resultData = jsonData;
        resultData = resultData.replace("getUserList:", "");
        QJsonDocument jsonDoc = QJsonDocument::fromJson(resultData.toUtf8());

        if (jsonDoc.isArray()) {
            QJsonArray jsonArray = jsonDoc.array();

            for(int i = 0; i < jsonArray.count(); i++) {
                QJsonObject o = jsonArray.at(i).toObject();

                m_listWidgetItem = new QListWidgetItem(QString("%1 %2 (%3)")
                                                       .arg(o.value("userName").toString())
                                                       .arg(o.value("userSurname").toString())
                                                       .arg(o.value("userPostName").toString()), ui->userList);
                m_listWidgetItem->setFlags(m_listWidgetItem->flags() | Qt::ItemIsUserCheckable);
                m_listWidgetItem->setCheckState(Qt::Unchecked);
                m_listWidgetItem->setData(Qt::UserRole,
                                          o.value("userID").toString());

            }

        }
        else {
            QMessageBox::information(this,
                                     "EDMS CLient",
                                     "Ошибка получения списка сотрудников! ");
        }
    }
    else {
        QMessageBox::information(this,
                                 "EDMS CLient",
                                 "Ошибка получения списка сотрудников! ");
    }

    setEnabledFunctional(true);
}

void CreateNewDocumentWindow::onCreateNewDocumentFinished(const QString &jsonData)
{
    setEnabledFunctional(true);
    if (m_jsonParse.getMethodName(jsonData) == "createNewDocument" &&
            m_jsonParse.getServerErrorCode(jsonData) == 0) {
        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Новый документ успешно создан! ");
        this->close();
    }
    else {
        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Произошла ошибка при создании нового документа! ");
    }
}

void CreateNewDocumentWindow::on_createDocumentButton_clicked()
{
    if (!ui->documentNameLine->text().isEmpty() &&
            !ui->documentShortDescriptionEdit->toPlainText().isEmpty()) {

        if (!m_documentPath.isNull() && !m_documentPath.isEmpty()) {

            bool isUserChoosen = false;
            for(int i = 0; i < ui->userList->count(); i++) {
                if (ui->userList->item(i)->checkState() == Qt::Checked) {
                    isUserChoosen = true;
                    break;
                }
            }

            if (isUserChoosen) {
                setEnabledFunctional(false);

                QByteArray documentName;
                documentName.append(ui->documentNameLine->text());
                documentName = documentName.toBase64();

                QByteArray documentShortDescription;
                documentShortDescription.append(ui->documentShortDescriptionEdit->toPlainText());
                documentShortDescription = documentShortDescription.toBase64();

                QString usersChoosen;
                for(int i = 0; i < ui->userList->count(); i++) {
                    if (ui->userList->item(i)->checkState() == Qt::Checked) {
                        usersChoosen.append(QString("%1,")
                                            .arg(ui->userList->item(i)->data(Qt::UserRole).toString()));
                    }
                }

                usersChoosen.chop(1);

                QByteArray chooseUsers;
                chooseUsers.append(usersChoosen);
                chooseUsers = chooseUsers.toBase64();

                QByteArray f;
                QByteArray b;
                if (!m_documentPath.isNull() && !m_documentPath.isEmpty()) {
                    QFile file;
                    file.setFileName(m_documentPath);
                    file.open(QIODevice::ReadOnly);
                    f.append(file.readAll());
                    file.close();

                    f = f.toBase64();

                    b.clear();
                    b.append(m_documentFormat);
                    b = b.toBase64();
                }

                m_tcpClient->createNewDocument(m_appStorage->getUserToken(),
                                               QString::fromLocal8Bit(documentName),
                                               QString::fromLocal8Bit(documentShortDescription),
                                               QString::fromLocal8Bit(f),
                                               QString::fromLocal8Bit(chooseUsers),
                                               QString::fromLocal8Bit(b));
            }
            else {
                QMessageBox::information(this,
                                         "EDMS Client",
                                         "Выберите хотя бы одного сотрудника! ");
            }
        }
        else {
            QMessageBox::information(this,
                                     "EDMS Client",
                                     "Выберите файл документа! ");
        }
    }
    else {
        if (ui->documentNameLine->text().isEmpty()) {
            QMessageBox::information(this,
                                     "EDMS CLient",
                                     "Введите название документа! ");
        }

        if (ui->documentShortDescriptionEdit->toPlainText().isEmpty()) {
            QMessageBox::information(this,
                                     "EDSM CLient",
                                     "Введите краткое описание документа! ");
        }
    }
}

void CreateNewDocumentWindow::on_browseButton_clicked()
{
    m_documentPath.clear();
    m_documentPath = QFileDialog::getOpenFileName(this,
                                                  "EDMS CLient",
                                                  "",
                                                  "Word files (*.doc *.docx);;"
                                                  "Excel files (*.xls *xlsx);;"
                                                  "Images (*.jpg *jpeg *png);;"
                                                  "PDF Files (*.pdf);;"
                                                  "Archives (*.zip *.rar *.7z)");
    if (!m_documentPath.isEmpty()) {
        QStringList sl = m_documentPath.split(".");
        m_documentFormat.clear();
        m_documentFormat = sl.at(1);
    }

    ui->filePathLine->setText(m_documentPath);
}

#ifndef CREATENEWDOCUMENTWINDOW_H
#define CREATENEWDOCUMENTWINDOW_H

#include <QDialog>

#include <QMessageBox>
#include <QListWidgetItem>
#include <QByteArray>
#include <QFileDialog>
#include <QFile>

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonObject>
#include <QVariant>

#include <QDebug>
#include "coreclass.h"
#include "jsonParse/jsonparse.h"

namespace Ui {
class CreateNewDocumentWindow;
}

class CreateNewDocumentWindow : public QDialog
{
    Q_OBJECT

public:
    explicit CreateNewDocumentWindow(QWidget *parent = nullptr);
    ~CreateNewDocumentWindow();

    void closeEvent(QCloseEvent *event);

private slots:
    void onGetUserListFinished(const QString &jsonData);
    void onCreateNewDocumentFinished(const QString &jsonData);

    void on_createDocumentButton_clicked();
    void on_browseButton_clicked();

signals:
    void closeWindow();

private:
    Ui::CreateNewDocumentWindow *ui;

    QString m_documentPath;
    QString m_documentFormat;
    QListWidgetItem *m_listWidgetItem;
    JsonParse m_jsonParse;
    void setEnabledFunctional(const bool &isEnabled);
};

#endif // CREATENEWDOCUMENTWINDOW_H

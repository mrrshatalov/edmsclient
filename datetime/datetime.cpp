#include "datetime.h"
DateTime::DateTime(QObject *parent) : QObject(parent) { }
DateTime::~DateTime() { }

QString DateTime::getHumanDateTimeText(const QString &dateTimeText)
{
    QDate date = QDate::fromString(dateTimeText.mid(0, 10), "yyyy-MM-dd");
    QTime time = QTime::fromString(dateTimeText.mid(11, 5), "hh:mm");
    return QString("%1 %2")
            .arg(date.toString("dd.MM.yyyy"))
            .arg(time.toString("hh:mm"));
}

QString DateTime::getHumanDateText(const QString &dateText)
{
    QDate date = QDate::fromString(dateText.mid(0, 10), "yyyy-MM-dd");
    return date.toString("dd.MM.yyyy");
}

QString DateTime::getHumanTimeText(const QString &timeText)
{
    QTime time = QTime::fromString(timeText.mid(11, 5), "hh:mm");
    return time.toString("hh:mm");
}

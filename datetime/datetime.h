#ifndef DATETIME_H
#define DATETIME_H

#include <QObject>

#include <QString>
#include <QDate>
#include <QTime>

class DateTime : public QObject
{
    Q_OBJECT
public:
    explicit DateTime(QObject *parent = nullptr);
    ~DateTime();

    QString getHumanDateTimeText(const QString &dateTimeText);
    QString getHumanDateText(const QString &dateText);
    QString getHumanTimeText(const QString &timeText);

};

#endif // DATETIME_H

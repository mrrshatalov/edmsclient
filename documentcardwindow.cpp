#include "documentcardwindow.h"
#include "ui_documentcardwindow.h"

#define m_tcpClient CoreClass::instance()->tcpClient()
#define m_appStorage CoreClass::instance()->appStorage()

DocumentCardWindow::DocumentCardWindow(const int &documentID,
                                       const QString &documentName,
                                       const QString &documentShortDescription,
                                       const int &documentStatusID,
                                       const QString &documentStatusName,
                                       const QString &documentDateTime,
                                       QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DocumentCardWindow)
{
    setData(documentID,
            documentName,
            documentShortDescription,
            documentStatusID,
            documentStatusName,
            documentDateTime);

    ui->setupUi(this);

    ui->headerLabel->setText(QString("Документ №%1")
                             .arg(documentID));
    ui->dateTimeCreateLabel->setText(documentDateTime);
    ui->documentStatusTextLabel->setText(documentStatusName);
    ui->documentNameEdit->setPlainText(documentName);
    ui->documentShortDescriptionEdit->setPlainText(documentShortDescription);
    ui->documentStatusTextLabel->setWordWrap(true);

    if (documentStatusID == 3 || documentStatusID == 4) {
        ui->commentButton->setEnabled(false);
        ui->rejectButton->setEnabled(false);

        ui->sendDocumentButton->setEnabled(false);
    }

    connect(m_tcpClient, SIGNAL(authorRejectDocumentFinished(QString)),
            this, SLOT(onAuthorRejectDocumentFinished(QString)));
    connect(m_tcpClient, SIGNAL(getDocumentHistoryFinished(QString)),
            this, SLOT(onGetDocumentHistoryFinished(QString)));
    connect(m_tcpClient, SIGNAL(authorCommentDocumentFinished(QString)),
            this, SLOT(onAuthorCommentDocumentFinished(QString)));
    connect(m_tcpClient, SIGNAL(getUsersDocumentStageDataFinished(QString)),
            this, SLOT(onGetUsersDocumentStageDataFinished(QString)));
    connect(m_tcpClient, SIGNAL(uploadDocumentFinished(QString)),
            this, SLOT(onUploadDocumentFinished(QString)));
    connect(m_tcpClient, SIGNAL(downloadDocumentFinished(QString)),
            this, SLOT(onDownloadDocumentFinished(QString)));

    m_tcpClient->getDocumentHistory(m_documentID);
}

DocumentCardWindow::~DocumentCardWindow()
{
    delete ui;
}

void DocumentCardWindow::closeEvent(QCloseEvent *event)
{
    emit closeWindow();
}

void DocumentCardWindow::setEnabledFunctional(const bool &isEnabled)
{
    ui->getDocumentButton->setEnabled(isEnabled);
    ui->sendDocumentButton->setEnabled(isEnabled);
    ui->rejectButton->setEnabled(isEnabled);
    ui->commentButton->setEnabled(isEnabled);
}

void DocumentCardWindow::setData(const int &documentID,
                                 const QString &documentName,
                                 const QString &documentShortDescription,
                                 const int &documentStatusID,
                                 const QString &documentStatusName,
                                 const QString &documentDateTime)
{
    m_documentID = documentID;
    m_documentName = documentName;
    m_documentShortDescription = documentShortDescription;
    m_documentStatusID = documentStatusID;
    m_documentStatusName = documentStatusName;
    m_documentDateTime = documentDateTime;
}

void DocumentCardWindow::onGetDocumentHistoryFinished(const QString &jsonData)
{
    m_tcpClient->getUsersDocumentStageData(m_appStorage->getUserToken(),
                                           m_documentID);

    if (jsonData.indexOf("getDocumentHistory", Qt::CaseInsensitive) != -1) {
        QString resultData = jsonData;
        resultData = resultData.replace("getDocumentHistory:", "");
        QJsonDocument jsonDoc = QJsonDocument::fromJson(resultData.toUtf8());

        if (jsonDoc.isArray()) {
            QJsonArray jsonArray = jsonDoc.array();

            ui->documentHistoryTableWidget->setRowCount(jsonArray.count());
            ui->documentHistoryTableWidget->setColumnCount(4);
            ui->documentHistoryTableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

            QStringList tableHeader;
            tableHeader << "Стадия" << "Комментарий" << "Дата/время" << "Сотрудник";
            ui->documentHistoryTableWidget->setHorizontalHeaderLabels(tableHeader);

            for(int i = 0; i < jsonArray.count(); i++) {
                QJsonObject o = jsonArray.at(i).toObject();

                ui->documentHistoryTableWidget->setItem(i,
                                                        0,
                                                        new QTableWidgetItem(o.value("textDocumentStageStatusName").toString()));
                ui->documentHistoryTableWidget->setItem(i,
                                                        1,
                                                        new QTableWidgetItem(o.value("userComment").toString()));
                ui->documentHistoryTableWidget->setItem(i,
                                                        2,
                                                        new QTableWidgetItem(m_dateTime.getHumanDateTimeText(o.value("textDocumentStageStatusDateTime").toString())));
                ui->documentHistoryTableWidget->setItem(i,
                                                        3,
                                                        new QTableWidgetItem(QString("%1 %2")
                                                                             .arg(o.value("userName").toString())
                                                                             .arg(o.value("userSurname").toString())));
            }
        }
        else {
            QMessageBox::information(this,
                                     "EDMS CLient",
                                     "Ошибка получения истории документа! ");
        }
    }
    else {
        QMessageBox::information(this,
                                 "EDMS CLient",
                                 "Ошибка получения истории документа! ");
    }
}

void DocumentCardWindow::onAuthorRejectDocumentFinished(const QString &jsonData)
{
    setEnabledFunctional(true);

    if (m_jsonParse.getMethodName(jsonData) == "authorRejectDocument" &&
            m_jsonParse.getServerErrorCode(jsonData) == 0) {
        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Документ был успешно отклонен! ");
        emit documentRejected(m_documentID);
    }
    else {
        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Произошла ошибка при отклонении документа! \n"
                                 "Повторите попытку снова. ");
    }
}

void DocumentCardWindow::onAuthorCommentDocumentFinished(const QString &jsonData)
{
    setEnabledFunctional(true);

    if (m_jsonParse.getMethodName(jsonData) == "authorCommentDocument" &&
            m_jsonParse.getServerErrorCode(jsonData) == 0) {
        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Ваши уточнения по документу были успешно отправлены! ");
        emit closeWindow();
    }
    else {
        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Произошла ошибка при отправке уточнений по документу! \n"
                                 "Повторите попытку снова. ");
    }
}

void DocumentCardWindow::onGetUsersDocumentStageDataFinished(const QString &jsonData)
{
    if (jsonData.indexOf("getUsersDocumentStageData", Qt::CaseInsensitive) != -1) {
        QString resultData = jsonData;
        resultData = resultData.replace("getUsersDocumentStageData:", "");
        QJsonDocument jsonDoc = QJsonDocument::fromJson(resultData.toUtf8());

        if (jsonDoc.isArray()) {
            QJsonArray jsonArray = jsonDoc.array();

            ui->userListTableWidget->setRowCount(jsonArray.count());
            ui->userListTableWidget->setColumnCount(5);
            ui->userListTableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

            QStringList tableHeader;
            tableHeader << "Код пользователя" << "Пользователь" << "Комментарий" << "Код стадии" << "Стадия" << "Дата/время";
            ui->userListTableWidget->setHorizontalHeaderLabels(tableHeader);
            ui->userListTableWidget->setColumnHidden(0, true);
            ui->userListTableWidget->setColumnHidden(3, true);

            for(int i = 0; i < jsonArray.count(); i++) {
                QJsonObject o = jsonArray.at(i).toObject();

                ui->userListTableWidget->setItem(i,
                                                 0,
                                                 new QTableWidgetItem(o.value("userID").toString()));
                ui->userListTableWidget->setItem(i,
                                                 1,
                                                 new QTableWidgetItem(QString("%1 %2")
                                                                      .arg(o.value("userName").toString())
                                                                      .arg(o.value("userSurname").toString())));
                ui->userListTableWidget->setItem(i,
                                                 2,
                                                 new QTableWidgetItem(o.value("userComment").toString()));
                ui->userListTableWidget->setItem(i,
                                                 3,
                                                 new QTableWidgetItem(o.value("textDocumentStageStatusID").toString()));
                ui->userListTableWidget->setItem(i,
                                                 4,
                                                 new QTableWidgetItem(o.value("textDocumentStageStatusName").toString()));
                ui->userListTableWidget->setItem(i,
                                                 5,
                                                 new QTableWidgetItem(m_dateTime.getHumanDateTimeText(o.value("textDocumentStageStatusDateTime").toString())));
            }

            bool isDocumentAccepted = true;
            for(int i = 0; i < ui->userListTableWidget->rowCount(); i++) {
                if (ui->userListTableWidget->item(i, 3)->text().toInt() != 1) {
                    isDocumentAccepted = false;
                    break;
                }
            }

            if (isDocumentAccepted && m_documentStatusID != 4) {
                ui->getQrCodeButton->setEnabled(true);
            }
        }
        else {
            QMessageBox::information(this,
                                     "EDMS CLient",
                                     "Ошибка получения данных о списке назначенных сотрудниках со стадией документа! ");
        }
    }
    else {
        QMessageBox::information(this,
                                 "EDMS CLient",
                                 "Ошибка получения данных о списке назначенных сотрудниках со стадией документа! ");
    }
}

void DocumentCardWindow::onUploadDocumentFinished(const QString &jsonData)
{
    if (m_jsonParse.getMethodName(jsonData) == "uploadDocument" &&
            m_jsonParse.getServerErrorCode(jsonData) == 0) {
        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Новая версия документа была успешно загружена! ");
        emit closeWindow();
    }
    else {
        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Произошла ошибка при загрузке новой версии документа! \n"
                                 "Повторите попытку снова. ");
    }
}

void DocumentCardWindow::on_rejectButton_clicked()
{
    bool ok;
    QString comment = QInputDialog::getText(this, tr("EDMS CLient"),
                                            tr("Краткий комментарий:"), QLineEdit::Normal,
                                            "",
                                            &ok);
    if (ok) {
        if (!comment.isEmpty()) {
            QByteArray b;
            b.append(comment);
            b = b.toBase64();

            setEnabledFunctional(false);
            m_tcpClient->authorRejectDocument(m_appStorage->getUserToken(),
                                              m_documentID,
                                              QString::fromLocal8Bit(b));
        }
        else {
            QMessageBox::information(this,
                                     "EDMS Client",
                                     "Необходимо ввести краткий комментарий! ");
        }
    }
}

void DocumentCardWindow::on_commentButton_clicked()
{
    bool ok;
    QString comment = QInputDialog::getText(this, tr("EDMS CLient"),
                                            tr("Краткий комментарий:"), QLineEdit::Normal,
                                            "",
                                            &ok);
    if (ok) {
        if (!comment.isEmpty()) {
            QByteArray b;
            b.append(comment);
            b = b.toBase64();

            setEnabledFunctional(false);
            m_tcpClient->authorCommentDocument(m_appStorage->getUserToken(),
                                               m_documentID,
                                               QString::fromLocal8Bit(b));
        }
        else {
            QMessageBox::information(this,
                                     "EDMS Client",
                                     "Необходимо ввести краткий комментарий! ");
        }
    }
}

void DocumentCardWindow::on_sendDocumentButton_clicked()
{
    m_documentPath.clear();
    m_documentPath = QFileDialog::getOpenFileName(this,
                                                  "EDMS CLient",
                                                  "",
                                                  "Word files (*.doc *.docx);;"
                                                  "Excel files (*.xls *xlsx);;"
                                                  "Images (*.jpg *jpeg *png);;"
                                                  "PDF Files (*.pdf);;"
                                                  "Archives (*.zip *.rar *.7z)");

    if (!m_documentPath.isEmpty()) {
        QStringList sl = m_documentPath.split(".");
        m_documentFormat.clear();
        m_documentFormat = sl.at(1);

        QByteArray fileData;
        fileData.clear();
        QByteArray fileFormatData;
        fileFormatData.clear();

        QFile file;
        file.setFileName(m_documentPath);
        file.open(QIODevice::ReadOnly);
        fileData.append(file.readAll());
        file.close();

        fileData = fileData.toBase64();

        fileFormatData.append(m_documentFormat);
        fileFormatData = fileFormatData.toBase64();

        bool ok;
        QString comment = QInputDialog::getText(this, tr("EDMS CLient"),
                                                tr("Краткий комментарий:"), QLineEdit::Normal,
                                                "",
                                                &ok);
        if (ok && !comment.isEmpty()) {
            QByteArray b;
            b.append(comment);
            b = b.toBase64();

            setEnabledFunctional(false);
            m_tcpClient->uploadDocument(m_appStorage->getUserToken(),
                                        m_documentID,
                                        QString::fromLocal8Bit(fileData),
                                        QString::fromLocal8Bit(fileFormatData),
                                        QString::fromLocal8Bit(b));
        }
    }
}

void DocumentCardWindow::on_getDocumentButton_clicked()
{
    m_documentPath = QFileDialog::getExistingDirectory(this,
                                                       "EDMS Client",
                                                       "");
    if (!m_documentPath.isEmpty()) {
        m_tcpClient->downloadDocument(m_appStorage->getUserToken(),
                                      m_documentID);
    }
}

void DocumentCardWindow::onDownloadDocumentFinished(const QString &jsonData)
{
    if (jsonData.indexOf("downloadDocument", Qt::CaseInsensitive) != -1) {
        QString resultData = jsonData;
        resultData = resultData.replace("downloadDocument:", "");

        QByteArray documentData;
        documentData.append(m_jsonParse.getJsonObjectData(resultData, "textDocumentDocument").toString());
        QString documentFormat = m_jsonParse.getJsonObjectData(resultData, "textDocumentDocumentFormat").toString();
        resultData.clear();

        QFile f;
        f.setFileName(QString("%1/%2.%3")
                      .arg(m_documentPath)
                      .arg(QString("document_%1").arg(m_documentID))
                      .arg(documentFormat));
        f.open(QIODevice::WriteOnly);
        f.write(QByteArray::fromBase64(documentData));
        f.close();

        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Файл успешно сохранен! ");
    }
    else {
        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Произошла ошибка при получении файла! ");
    }
}

void DocumentCardWindow::on_getQrCodeButton_clicked()
{
    QString qrData = QString("%1_%2_%3")
            .arg(m_documentID)
            .arg(m_documentName)
            .arg(QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss:ms"));

    QByteArray b;
    b.clear();
    b.append(qrData);
    b = b.toBase64();

    qrData.clear();
    qrData.append(b);
    b.clear();

    QImage barcodeImage = QZXing::encodeData(qrData,
                                             QZXing::EncoderFormat_QR_CODE,
                                             QSize(250, 250),
                                             QZXing::EncodeErrorCorrectionLevel_H);

    QBuffer buffer(&b);
    buffer.open(QIODevice::WriteOnly);
    barcodeImage.save(&buffer, "PNG");

    m_qrcnw = new QrCodeNotificationWindow(QString::fromLatin1(b.toBase64().data()),
                                           m_documentID,
                                           NULL);
    connect(m_qrcnw, SIGNAL(closeWindow()),
            m_qrcnw, SLOT(deleteLater()));
    m_qrcnw->show();
}

#ifndef DOCUMENTCARDWINDOW_H
#define DOCUMENTCARDWINDOW_H

#include <QDialog>

#include "QMessageBox"
#include <QInputDialog>
#include <QFileDialog>
#include <QFile>
#include <QStringList>

#include <QByteArray>
#include <QBuffer>
#include <QImage>

#include "coreclass.h"
#include "jsonParse/jsonparse.h"
#include "datetime/datetime.h"
#include "QZXing.h"
#include "qrcodenotificationwindow.h"

namespace Ui {
class DocumentCardWindow;
}

class DocumentCardWindow : public QDialog
{
    Q_OBJECT

public:
    explicit DocumentCardWindow(const int &documentID,
                                const QString &documentName,
                                const QString &documentShortDescription,
                                const int &documentStatusID,
                                const QString &documentStatusName,
                                const QString &documentDateTime,
                                QWidget *parent = nullptr);
    ~DocumentCardWindow();
    void closeEvent(QCloseEvent *event);

signals:
    void documentRejected(const int &documentID);
    void closeWindow();

private slots:
    void onGetDocumentHistoryFinished(const QString &jsonData);
    void onAuthorRejectDocumentFinished(const QString &jsonData);
    void onAuthorCommentDocumentFinished(const QString &jsonData);
    void onGetUsersDocumentStageDataFinished(const QString &jsonData);
    void onUploadDocumentFinished(const QString &jsonData);
    void onDownloadDocumentFinished(const QString &jsonData);

    void on_rejectButton_clicked();
    void on_commentButton_clicked();

    void on_sendDocumentButton_clicked();

    void on_getDocumentButton_clicked();

    void on_getQrCodeButton_clicked();

private:
    Ui::DocumentCardWindow *ui;

    int m_documentID;
    QString m_documentName;
    QString m_documentShortDescription;
    int m_documentStatusID;
    QString m_documentStatusName;
    QString m_documentDateTime;

    void setData(const int &documentID,
                 const QString &documentName,
                 const QString &documentShortDescription,
                 const int &documentStatusID,
                 const QString &documentStatusName,
                 const QString &documentDateTime);

    JsonParse m_jsonParse;

    QString m_documentPath;
    QString m_documentFormat;

    DateTime m_dateTime;

    QrCodeNotificationWindow *m_qrcnw;

    void setEnabledFunctional(const bool &isEnabled);
};

#endif // DOCUMENTCARDWINDOW_H

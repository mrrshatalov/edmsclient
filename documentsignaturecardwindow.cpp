#include "documentsignaturecardwindow.h"
#include "ui_documentsignaturecardwindow.h"

#define m_tcpClient CoreClass::instance()->tcpClient()
#define m_appStorage CoreClass::instance()->appStorage()

DocumentSignatureCardWindow::DocumentSignatureCardWindow(const int &documentID,
                                                         const QString &documentName,
                                                         const QString &documentShortDescription,
                                                         const int &documentStatusID,
                                                         const QString &documentStatusName,
                                                         const QString &documentDateTime,
                                                         const QString &documentAuthor,
                                                         const int &documentAuthorID,
                                                         const int &documentStageLastStatusID,
                                                         QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DocumentSignatureCardWindow)
{
    setData(documentID,
            documentName,
            documentShortDescription,
            documentStatusID,
            documentStatusName,
            documentDateTime,
            documentAuthor,
            documentAuthorID,
            documentStageLastStatusID);

    ui->setupUi(this);
    ui->documentStatusTextLabel->setWordWrap(true);

    ui->headerLabel->setText(QString("Документ №%1")
                             .arg(documentID));
    ui->dateTimeCreateLabel->setText(documentDateTime);
    ui->documentStatusTextLabel->setText(documentStatusName);
    ui->authorTextLabel->setText(documentAuthor);
    ui->documentNameEdit->setPlainText(documentName);
    ui->documentShortDescriptionEdit->setPlainText(documentShortDescription);

    if ((documentStatusID == 3 || documentStatusID == 4) ||
            (documentStageLastStatusID == 1 || documentStageLastStatusID == 2)) {
        ui->commentButton->setEnabled(false);
        ui->rejectButton->setEnabled(false);
        ui->acceptButton->setEnabled(false);
    }

    connect(m_tcpClient, SIGNAL(rejectDocumentFinished(QString)),
            this, SLOT(onRejectDocumentFinished(QString)));
    connect(m_tcpClient, SIGNAL(getDocumentHistoryFinished(QString)),
            this, SLOT(onGetDocumentHistoryFinished(QString)));
    connect(m_tcpClient, SIGNAL(commentDocumentFinished(QString)),
            this, SLOT(onCommentDocumentFinished(QString)));
    connect(m_tcpClient, SIGNAL(acceptDocumentFinished(QString)),
            this, SLOT(onAcceptDocumentFinished(QString)));
    connect(m_tcpClient, SIGNAL(downloadDocumentFinished(QString)),
            this, SLOT(onDownloadDocumentFinished(QString)));
    connect(m_tcpClient, SIGNAL(downloadDocumentByStageFinished(QString)),
            this, SLOT(onDownloadDocumentByStageFinished(QString)));
    m_tcpClient->getDocumentHistory(m_documentID);
}

DocumentSignatureCardWindow::~DocumentSignatureCardWindow()
{
    delete ui;
}

void DocumentSignatureCardWindow::closeEvent(QCloseEvent *event)
{
    emit closeWindow();
}

void DocumentSignatureCardWindow::setEnabledFunctional(const bool &isEnabled)
{
    ui->rejectButton->setEnabled(isEnabled);
    ui->commentButton->setEnabled(isEnabled);
    ui->acceptButton->setEnabled(isEnabled);

    ui->getDocumentButton->setEnabled(isEnabled);
}

void DocumentSignatureCardWindow::setData(const int &documentID,
                                          const QString &documentName,
                                          const QString &documentShortDescription,
                                          const int &documentStatusID,
                                          const QString &documentStatusName,
                                          const QString &documentDateTime,
                                          const QString &documentAuthor,
                                          const int &documentAuthorID,
                                          const int &documentStageLastStatusID)
{
    m_documentID = documentID;
    m_documentName = documentName;
    m_documentShortDescription = documentShortDescription;
    m_documentStatusID = documentStatusID;
    m_documentStatusName = documentStatusName;
    m_documentDateTime = documentDateTime;
    m_documentAuthor = documentAuthor;
    m_documentAuthorID = documentAuthorID;
    m_documentStageLastStatusID = documentStageLastStatusID;
}

void DocumentSignatureCardWindow::onGetDocumentHistoryFinished(const QString &jsonData)
{
    if (jsonData.indexOf("getDocumentHistory", Qt::CaseInsensitive) != -1) {
        QString resultData = jsonData;
        resultData = resultData.replace("getDocumentHistory:", "");
        QJsonDocument jsonDoc = QJsonDocument::fromJson(resultData.toUtf8());

        if (jsonDoc.isArray()) {
            QJsonArray jsonArray = jsonDoc.array();

            ui->documentHistoryTableWidget->setRowCount(jsonArray.count());
            ui->documentHistoryTableWidget->setColumnCount(7);
            ui->documentHistoryTableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

            QStringList tableHeader;
            tableHeader << "Код стадии" << "Стадия" << "Комментарий" <<
                           "Дата/время" << "Сотрудник" << "Документ" <<
                           "Код сотрудника";
            ui->documentHistoryTableWidget->setHorizontalHeaderLabels(tableHeader);
            ui->documentHistoryTableWidget->setColumnHidden(0, true);
            ui->documentHistoryTableWidget->setColumnHidden(6, true);

            for(int i = 0; i < jsonArray.count(); i++) {
                QJsonObject o = jsonArray.at(i).toObject();

                ui->documentHistoryTableWidget->setItem(i,
                                                        0,
                                                        new QTableWidgetItem(o.value("textDocumentStageID").toString()));
                ui->documentHistoryTableWidget->setItem(i,
                                                        1,
                                                        new QTableWidgetItem(o.value("textDocumentStageStatusName").toString()));
                ui->documentHistoryTableWidget->setItem(i,
                                                        2,
                                                        new QTableWidgetItem(o.value("userComment").toString()));
                ui->documentHistoryTableWidget->setItem(i,
                                                        3,
                                                        new QTableWidgetItem(m_dateTime.getHumanDateTimeText(o.value("textDocumentStageStatusDateTime").toString())));
                ui->documentHistoryTableWidget->setItem(i,
                                                        4,
                                                        new QTableWidgetItem(QString("%1 %2")
                                                                             .arg(o.value("userName").toString())
                                                                             .arg(o.value("userSurname").toString())));

                ui->documentHistoryTableWidget->setItem(i,
                                                        6,
                                                        new QTableWidgetItem(o.value("userID").toString()));

                if (QString("%1").arg(o.value("userID").toString()).toInt() == m_documentAuthorID &&
                        o.value("isTextDocumentThere").toString().toInt() == 1)  {
                    m_downloadDocumentButton = new QPushButton("Скачать", ui->documentHistoryTableWidget);
                    connect(m_downloadDocumentButton, SIGNAL(clicked()),
                            this, SLOT(onDownloadDocumentButtonClicked()));
                    ui->documentHistoryTableWidget->setCellWidget(i, 5, m_downloadDocumentButton);
                }
            }
        }
        else {
            QMessageBox::information(this,
                                     "EDMS CLient",
                                     "Ошибка получения истории документа! ");
        }
    }
    else {
        QMessageBox::information(this,
                                 "EDMS CLient",
                                 "Ошибка получения истории документа! ");
    }
}

void DocumentSignatureCardWindow::on_rejectButton_clicked()
{
    bool ok;
    QString comment = QInputDialog::getText(this, tr("EDMS CLient"),
                                            tr("Краткий комментарий:"), QLineEdit::Normal,
                                            "",
                                            &ok);
    if (ok) {
        if (!comment.isEmpty()) {
            QByteArray b;
            b.append(comment);
            b = b.toBase64();

            setEnabledFunctional(false);
            m_tcpClient->rejectDocument(m_appStorage->getUserToken(),
                                        m_documentID,
                                        QString::fromLocal8Bit(b));
        }
        else {
            QMessageBox::information(this,
                                     "EDMS Client",
                                     "Необходимо ввести краткий комментарий! ");
        }
    }
}

void DocumentSignatureCardWindow::onRejectDocumentFinished(const QString &jsonData)
{
    setEnabledFunctional(true);

    if (m_jsonParse.getMethodName(jsonData) == "rejectDocument" &&
            m_jsonParse.getServerErrorCode(jsonData) == 0) {
        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Документ был успешно отклонен! ");
        emit documentRejected(m_documentID);
    }
    else {
        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Произошла ошибка при отклонении документа! \n"
                                 "Повторите попытку снова. ");
    }
}

void DocumentSignatureCardWindow::onCommentDocumentFinished(const QString &jsonData)
{
    setEnabledFunctional(true);

    if (m_jsonParse.getMethodName(jsonData) == "commentDocument" &&
            m_jsonParse.getServerErrorCode(jsonData) == 0) {
        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Замечания по документу были успешно отправлены! ");
        emit documentCommented(m_documentID);
    }
    else {
        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Произошла ошибка при отправке замечаний по документу! \n"
                                 "Повторите попытку снова. ");
    }
}

void DocumentSignatureCardWindow::onAcceptDocumentFinished(const QString &jsonData)
{
    setEnabledFunctional(true);

    if (m_jsonParse.getMethodName(jsonData) == "acceptDocument" &&
            m_jsonParse.getServerErrorCode(jsonData) == 0) {
        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Ваше мнение учтено! ");
        emit documentAccepted(m_documentID);
    }
    else {
        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Произошла ошибка при отправке запроса! \n"
                                 "Повторите попытку снова. ");
    }
}

void DocumentSignatureCardWindow::onDownloadDocumentFinished(const QString &jsonData)
{
    if (jsonData.indexOf("downloadDocument", Qt::CaseInsensitive) != -1) {
        QString resultData = jsonData;
        resultData = resultData.replace("downloadDocument:", "");

        QByteArray documentData;
        documentData.append(m_jsonParse.getJsonObjectData(resultData, "textDocumentDocument").toString());
        QString documentFormat = m_jsonParse.getJsonObjectData(resultData, "textDocumentDocumentFormat").toString();
        resultData.clear();

        QFile f;
        f.setFileName(QString("%1/%2.%3")
                      .arg(m_documentPath)
                      .arg(QString("document_%1").arg(m_documentID))
                      .arg(documentFormat));
        f.open(QIODevice::WriteOnly);
        f.write(QByteArray::fromBase64(documentData));
        f.close();

        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Файл успешно сохранен! ");
    }
    else {
        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Произошла ошибка при получении файла! ");
    }
}

void DocumentSignatureCardWindow::onDownloadDocumentByStageFinished(const QString &jsonData)
{
    if (jsonData.indexOf("stageDocumentDownload", Qt::CaseInsensitive) != -1 &&
            jsonData.indexOf("{", Qt::CaseInsensitive != -1) &&
            jsonData.indexOf("}", Qt::CaseInsensitive != -1)) {
        QString resultData = jsonData;
        resultData = resultData.replace("stageDocumentDownload:", "");

        QByteArray documentData;
        documentData.append(m_jsonParse.getJsonObjectData(resultData, "textDocumentDocument").toString());
        QString documentFormat = m_jsonParse.getJsonObjectData(resultData, "textDocumentDocumentFormat").toString();
        resultData.clear();

        QString documentUploadDateTime = m_jsonParse.getJsonObjectData(jsonData, "textDocumentStageStatusDateTime").toString();
        QDate date = QDate::fromString(documentUploadDateTime.mid(0, 10), "yyyy-MM-dd");
        QTime time = QTime::fromString(documentUploadDateTime.mid(11, 5), "hh:mm");

        QFile f;
        f.setFileName(QString("%1/%2.%3")
                      .arg(m_documentPath)
                      .arg(QString("document_%1_%2_%3")
                           .arg(m_documentID)
                           .arg(date.toString("ddMMyyyy"))
                           .arg(time.toString("hhmm")))
                      .arg(documentFormat));
        f.open(QIODevice::WriteOnly);
        f.write(QByteArray::fromBase64(documentData));
        f.close();

        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Файл успешно сохранен! ");
    }
    else {
        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Произошла ошибка при получении файла! ");
    }
}

void DocumentSignatureCardWindow::on_commentButton_clicked()
{
    bool ok;
    QString comment = QInputDialog::getText(this, tr("EDMS CLient"),
                                            tr("Краткий комментарий:"), QLineEdit::Normal,
                                            "",
                                            &ok);
    if (ok) {
        if (!comment.isEmpty()) {
            QByteArray b;
            b.append(comment);
            b = b.toBase64();

            setEnabledFunctional(false);
            m_tcpClient->commentDocument(m_appStorage->getUserToken(),
                                         m_documentID,
                                         QString::fromLocal8Bit(b));
        }
    }
    else {
        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Необходимо ввести краткий комментарий! ");
    }
}

void DocumentSignatureCardWindow::on_acceptButton_clicked()
{
    bool ok;
    QString comment = QInputDialog::getText(this, tr("EDMS CLient"),
                                            tr("Краткий комментарий (не обязательно):"), QLineEdit::Normal,
                                            "",
                                            &ok);
    if (ok) {
        QByteArray b;
        b.append(comment);
        b = b.toBase64();

        setEnabledFunctional(false);
        m_tcpClient->acceptDocument(m_appStorage->getUserToken(),
                                    m_documentID,
                                    QString::fromLocal8Bit(b));
    }
}

void DocumentSignatureCardWindow::on_getDocumentButton_clicked()
{
    m_documentPath = QFileDialog::getExistingDirectory(this,
                                                       "EDMS Client",
                                                       "");
    if (!m_documentPath.isEmpty()) {
        m_tcpClient->downloadDocument(m_appStorage->getUserToken(),
                                      m_documentID);
    }
}

void DocumentSignatureCardWindow::onDownloadDocumentButtonClicked()
{
    for(int rowNumber = 0; rowNumber < ui->documentHistoryTableWidget->rowCount(); rowNumber++){
        if(sender() == ui->documentHistoryTableWidget->cellWidget(rowNumber, 5)) {
            m_documentPath = QFileDialog::getExistingDirectory(this,
                                                               "EDMS Client",
                                                               "");
            if (!m_documentPath.isEmpty()) {
                m_tcpClient->downloadDocumentByStage(m_appStorage->getUserToken(),
                                                     ui->documentHistoryTableWidget->item(rowNumber, 6)->text().toInt(),
                                                     m_documentID,
                                                     ui->documentHistoryTableWidget->item(rowNumber, 0)->text().toInt());
            }
        }
    }
}

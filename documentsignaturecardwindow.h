#ifndef DOCUMENTSIGNATURECARDWINDOW_H
#define DOCUMENTSIGNATURECARDWINDOW_H

#include <QDialog>

#include "QMessageBox"
#include <QInputDialog>
#include <QFileDialog>
#include <QPushButton>
#include <QDate>
#include <QTime>
#include <QCloseEvent>

#include "coreclass.h"
#include "jsonParse/jsonparse.h"
#include "datetime/datetime.h"

namespace Ui {
class DocumentSignatureCardWindow;
}

class DocumentSignatureCardWindow : public QDialog
{
    Q_OBJECT

public:
    explicit DocumentSignatureCardWindow(const int &documentID,
                                         const QString &documentName,
                                         const QString &documentShortDescription,
                                         const int &documentStatusID,
                                         const QString &documentStatusName,
                                         const QString &documentDateTime,
                                         const QString &documentAuthor,
                                         const int &documentAuthorID,
                                         const int &documentStageLastStatusID,
                                         QWidget *parent = nullptr);
    ~DocumentSignatureCardWindow();
    void closeEvent(QCloseEvent *event);

signals:
    void documentAccepted(const int &documentID);
    void documentRejected(const int &documentID);
    void documentCommented(const int &documentID);
    void closeWindow();

private slots:
    void on_rejectButton_clicked();

    void onRejectDocumentFinished(const QString &jsonData);
    void onGetDocumentHistoryFinished(const QString &jsonData);
    void onCommentDocumentFinished(const QString &jsonData);
    void onAcceptDocumentFinished(const QString &jsonData);
    void onDownloadDocumentFinished(const QString &jsonData);
    void onDownloadDocumentByStageFinished(const QString &jsonData);

    void on_commentButton_clicked();
    void on_acceptButton_clicked();
    void on_getDocumentButton_clicked();
    void onDownloadDocumentButtonClicked();

private:
    Ui::DocumentSignatureCardWindow *ui;


    int m_documentID;
    QString m_documentName;
    QString m_documentShortDescription;
    int m_documentStatusID;
    QString m_documentStatusName;
    QString m_documentDateTime;
    QString m_documentAuthor;
    int m_documentAuthorID;
    int m_documentStageLastStatusID;

    void setData(const int &documentID,
                 const QString &documentName,
                 const QString &documentShortDescription,
                 const int &documentStatusID,
                 const QString &documentStatusName,
                 const QString &documentDateTime,
                 const QString &documentAuthor,
                 const int &documentAuthorID,
                 const int &documentStageLastStatusID);

    JsonParse m_jsonParse;

    QString m_documentPath;

    QPushButton *m_downloadDocumentButton;

    DateTime m_dateTime;

    void setEnabledFunctional(const bool &isEnabled);
};

#endif // DOCUMENTSIGNATURECARDWINDOW_H

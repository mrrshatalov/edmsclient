#include "enterwindow.h"
#include "ui_enterwindow.h"

#define m_tcpClient CoreClass::instance()->tcpClient()
#define m_appStorage CoreClass::instance()->appStorage()

EnterWindow::EnterWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EnterWindow)
{
    ui->setupUi(this);

    m_settings = new QSettings(QString("%1/settings.ini")
                               .arg(qApp->applicationDirPath()),
                               QSettings::IniFormat);
    QString serverAddress = m_settings->value("serverAddress").toString();
    quint16 serverPort = m_settings->value("serverPort").toInt();

    connect(m_tcpClient, SIGNAL(userEntered(QString)),
            this, SLOT(onUserEntered(QString)));

    if (!serverAddress.isEmpty() && serverPort != 0) {
        m_tcpClient->startTCPClient(serverAddress, serverPort);
    }
    else {
        m_tcpClient->startTCPClient("127.0.0.1", 8090);
    }
}

EnterWindow::~EnterWindow()
{
    m_settings->deleteLater();
    delete ui;
}

void EnterWindow::on_enterButton_clicked()
{
    QString userLogin = ui->loginTextEdit->text();
    QString userPassword = ui->passwordTextEdit->text();

    if (userLogin.isEmpty() || userPassword.isEmpty()) {
        if (userLogin.isEmpty()) {
            QMessageBox::information(this,
                                     "EDMS Client",
                                     "Введите логин! ");
        }

        if (userPassword.isEmpty()) {
            QMessageBox::information(this,
                                     "EDMS Client",
                                     "Введите пароль! ");
        }
    }
    else {
        QCryptographicHash hash(QCryptographicHash::Md5);
        hash.addData(userPassword.toLocal8Bit());
        QString passwordHash(hash.result().toHex());

        m_tcpClient->userEnter(userLogin,
                               passwordHash);
    }
}

void EnterWindow::onUserEntered(const QString &jsonData)
{
    if (m_jsonParse.getMethodName(jsonData) == "userEnter" &&
            m_jsonParse.getServerErrorCode(jsonData) == 0) {

        m_appStorage->setUserID(m_jsonParse.getJsonObjectData(jsonData, "userID").toInt());
        m_appStorage->setUserName(m_jsonParse.getJsonObjectData(jsonData, "userName").toString());
        m_appStorage->setUserSurname(m_jsonParse.getJsonObjectData(jsonData, "userSurname").toString());
        m_appStorage->setUserPostName(m_jsonParse.getJsonObjectData(jsonData, "userPostName").toString());
        m_appStorage->setUserToken(m_jsonParse.getJsonObjectData(jsonData, "userToken").toString());

        m_mainWindow = new MainWindow();
        m_mainWindow->show();
        this->close();

    }
    else {
        QMessageBox::information(this,
                                 "EDMS Client",
                                 "Не правильная пара \"Логин/пароль!\"! \n"
                                 "Попробуйте еще раз. ");
    }
}

void EnterWindow::on_settingsButton_clicked()
{
    m_sw = new SettingsWindow(NULL);
    connect(m_sw, SIGNAL(closeWindow()),
            m_sw, SLOT(deleteLater()));
    m_sw->show();
}

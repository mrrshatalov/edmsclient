#ifndef ENTERWINDOW_H
#define ENTERWINDOW_H

#include <QDialog>
#include <QMessageBox>
#include "QCryptographicHash"
#include <QSettings>

#include "coreclass.h"
#include "jsonParse/jsonparse.h"
#include "mainwindow.h"
#include "settingswindow.h"

namespace Ui {
class EnterWindow;
}

class EnterWindow : public QDialog
{
    Q_OBJECT

public:
    explicit EnterWindow(QWidget *parent = nullptr);
    ~EnterWindow();

private slots:
    void on_enterButton_clicked();

    void onUserEntered(const QString &jsonData);

    void on_settingsButton_clicked();

private:
    Ui::EnterWindow *ui;
    JsonParse m_jsonParse;
    MainWindow *m_mainWindow;
    SettingsWindow *m_sw;
    QSettings *m_settings;
};

#endif // ENTERWINDOW_H

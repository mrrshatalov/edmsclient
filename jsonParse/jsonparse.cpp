#include "jsonparse.h"

JsonParse::JsonParse(QObject *parent) : QObject(parent) { }
JsonParse::~JsonParse() { }

//Получить код ошибки
int JsonParse::getServerErrorCode(const QString &jsonData)
{
    QString data = jsonData;
    if (!data.startsWith("{", Qt::CaseInsensitive)) {
        int startWith = data.indexOf("{");
        int endWith = data.indexOf("}");

        data = data.mid(startWith, (endWith - startWith)+1);
    }

    QJsonDocument jsonDoc = QJsonDocument::fromJson(data.toUtf8());
    QJsonObject jsonObj = jsonDoc.object();
    QJsonValue errorCode = jsonObj.value("errorCode");

    return errorCode.toInt();
}

QString JsonParse::getMethodName(const QString &jsonData)
{
    QString data = jsonData;
    if (!data.startsWith("{", Qt::CaseInsensitive)) {
        int startWith = data.indexOf("{");
        int endWith = data.indexOf("}");

        data = data.mid(startWith, (endWith - startWith)+1);
    }

    QJsonDocument jsonDoc = QJsonDocument::fromJson(data.toUtf8());
    QJsonObject jsonObj = jsonDoc.object();

    return jsonObj.value("methodName").toString();
}

//Получить объект JSON-документа
QVariant JsonParse::getJsonObjectData(const QString &jsonData,
                                      const QString &paramName)
{
    QString data = jsonData;
    if (!data.startsWith("{", Qt::CaseInsensitive)) {
        int startWith = data.indexOf("{");
        int endWith = data.indexOf("}");

        data = data.mid(startWith, (endWith - startWith)+1);
    }

    QJsonDocument jsonDoc = QJsonDocument::fromJson(data.toUtf8());
    QJsonObject jsonObj = jsonDoc.object();

    return jsonObj.value(paramName).toVariant();
}


#ifndef JSONPARSE_H
#define JSONPARSE_H

#include <QObject>

#include <QString>
#include <QVariant>

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

#include <QDebug>

class JsonParse : public QObject
{
    Q_OBJECT
public:
    explicit JsonParse(QObject *parent = nullptr);
    ~JsonParse();

    //Получить код ошибки
    int getServerErrorCode(const QString &jsonData);

    //Получить текст ошибки
    QString getMethodName(const QString &jsonData);

    //Получить объект JSON-документа
    QVariant getJsonObjectData(const QString &jsonData,
                               const QString &paramName);



};

#endif // JSONPARSE_H

#include "enterwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    EnterWindow ew;
    ew.show();
    return a.exec();
}

#include "mainwindow.h"
#include "ui_mainwindow.h"

#define m_tcpClient CoreClass::instance()->tcpClient()
#define m_appStorage CoreClass::instance()->appStorage()

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //Для сворачивания в трей
    m_trayIcon = new QSystemTrayIcon(this);
    m_trayIcon->setIcon(this->style()->standardIcon(QStyle::SP_ComputerIcon));
    m_trayIcon->setToolTip("EDMS Client");

    QMenu * menu = new QMenu(this);
    QAction *viewWindow = new QAction(trUtf8("Развернуть"), this);
    QAction *quitAction = new QAction(trUtf8("Выход"), this);

    connect(viewWindow, SIGNAL(triggered()), this, SLOT(show()));
    connect(quitAction, SIGNAL(triggered()), this, SLOT(closeApplication()));

    menu->addAction(viewWindow);
    menu->addAction(quitAction);

    m_trayIcon->setContextMenu(menu);
    m_trayIcon->show();

    connect(m_trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));
    //

    //Последние события
    m_lastEventsCount = 0;
    ui->lastEventsTableWidget->setRowCount(0);
    ui->lastEventsTableWidget->setColumnCount(3);
    ui->lastEventsTableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

    QStringList tableHeader;
    tableHeader << "Номер события" << "Текст события" << "Дата/время";
    ui->lastEventsTableWidget->setHorizontalHeaderLabels(tableHeader);
    //


    connect(m_tcpClient, SIGNAL(clientConnectedToServer(bool)),
            this, SLOT(onClientConnectedToServer(bool)));

    connect(m_tcpClient, SIGNAL(userQuited(QString)),
            this, SLOT(onUserQuited(QString)));

    connect(m_tcpClient, SIGNAL(newDocumentRecieved(QString)),
            this, SLOT(onNewDocumentRecieved(QString)));
    connect(m_tcpClient, SIGNAL(newDocumentAccepted(QString)),
            this, SLOT(onNewDocumentAccepted(QString)));
    connect(m_tcpClient, SIGNAL(newDocumentRejected(QString)),
            this, SLOT(onNewDocumentRejected(QString)));
    connect(m_tcpClient, SIGNAL(authorDocumentRejected(QString)),
            this, SLOT(onAuthorDocumentRejected(QString)));
    connect(m_tcpClient, SIGNAL(newDocumentCommented(QString)),
            this, SLOT(onNewDocumentCommented(QString)));
    connect(m_tcpClient, SIGNAL(authorDocumentCommented(QString)),
            this, SLOT(onAuthorDocumentCommented(QString)));
    connect(m_tcpClient, SIGNAL(newDocumentUploaded(QString)),
            this, SLOT(onNewDocumentUploaded(QString)));

    connect(m_tcpClient, SIGNAL(newEventsNotificationRecieved(QString)),
            this, SLOT(onNewEventsNotificationRecieved(QString)));

    ui->userNameSurnameTextLabel->setText(QString("%1 %2, %3 ")
                                          .arg(m_appStorage->getUserName().trimmed())
                                          .arg(m_appStorage->getUserSurname().trimmed())
                                          .arg(m_appStorage->getUserPostName()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if(this->isVisible()){
        event->ignore();
        this->hide();
        QSystemTrayIcon::MessageIcon icon = QSystemTrayIcon::MessageIcon(QSystemTrayIcon::Information);

        m_trayIcon->showMessage("EDSM CLient",
                                trUtf8("Приложение свернуто в трей..."),
                                icon,
                                2000);
    }
}

void MainWindow::closeApplication()
{
    m_tcpClient->userQuit(m_appStorage->getUserToken());
}

void MainWindow::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    if(!this->isVisible()) {
        this->show();
    }
}

void MainWindow::onUserQuited(const QString &jsonData)
{
    qApp->quit();
}

void MainWindow::onClientConnectedToServer(const bool &result)
{
    if (result) {
        m_tcpClient->userReEnter(m_appStorage->getUserToken());
    }
}

void MainWindow::onNewDocumentRecieved(const QString &jsonData)
{
    if (m_jsonParse.getMethodName(jsonData) == "newDocumentRecieved" &&
            m_jsonParse.getServerErrorCode(jsonData) == 0) {

        //Добавление информации в список последних событий
        m_lastEventsCount++;
        ui->lastEventsTableWidget->insertRow(0);
        ui->lastEventsTableWidget->setItem(0,
                                           0,
                                           new QTableWidgetItem(QString::number(m_lastEventsCount)));
        ui->lastEventsTableWidget->setItem(0,
                                           1,
                                           new QTableWidgetItem("Получен новый документ для рассмотрения"));
        ui->lastEventsTableWidget->setItem(0,
                                           2,
                                           new QTableWidgetItem(QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss")));

        ui->lastEventsTableWidget->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
        ui->lastEventsTableWidget->verticalHeader()->resizeSections(QHeaderView::ResizeToContents);
        //

        m_notificationWindow = new NotificationWindow("Получен новый документ для рассмотрения",
                                                      NULL);
        connect(m_notificationWindow, SIGNAL(closeWindow()),
                m_notificationWindow, SLOT(deleteLater()));
        m_notificationWindow->show();
    }
}

void MainWindow::onNewDocumentAccepted(const QString &jsonData)
{
    if (m_jsonParse.getMethodName(jsonData) == "newDocumentAccepted" &&
            m_jsonParse.getServerErrorCode(jsonData) == 0) {

        //Добавление информации в список последних событий
        m_lastEventsCount++;
        ui->lastEventsTableWidget->insertRow(0);
        ui->lastEventsTableWidget->setItem(0,
                                           0,
                                           new QTableWidgetItem(QString::number(m_lastEventsCount)));
        ui->lastEventsTableWidget->setItem(0,
                                           1,
                                           new QTableWidgetItem(QString("Подтверждение по документу №%1")
                                                                .arg(m_jsonParse.getJsonObjectData(jsonData, "documentID").toString().toInt())));
        ui->lastEventsTableWidget->setItem(0,
                                           2,
                                           new QTableWidgetItem(QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss")));

        ui->lastEventsTableWidget->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
        ui->lastEventsTableWidget->verticalHeader()->resizeSections(QHeaderView::ResizeToContents);
        //

        m_notificationWindow = new NotificationWindow(QString("Подтверждение по документу №%1")
                                                      .arg(m_jsonParse.getJsonObjectData(jsonData, "documentID").toString().toInt()),
                                                      NULL);
        connect(m_notificationWindow, SIGNAL(closeWindow()),
                m_notificationWindow, SLOT(deleteLater()));
        m_notificationWindow->show();
    }
}

void MainWindow::onNewDocumentRejected(const QString &jsonData)
{
    if (m_jsonParse.getMethodName(jsonData) == "newDocumentRejected" &&
            m_jsonParse.getServerErrorCode(jsonData) == 0) {

        //Добавление информации в список последних событий
        m_lastEventsCount++;
        ui->lastEventsTableWidget->insertRow(0);
        ui->lastEventsTableWidget->setItem(0,
                                           0,
                                           new QTableWidgetItem(QString::number(m_lastEventsCount)));
        ui->lastEventsTableWidget->setItem(0,
                                           1,
                                           new QTableWidgetItem(QString("Документ №%1 отклонен! ")
                                                                .arg(m_jsonParse.getJsonObjectData(jsonData, "documentID").toString().toInt())));
        ui->lastEventsTableWidget->setItem(0,
                                           2,
                                           new QTableWidgetItem(QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss")));

        ui->lastEventsTableWidget->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
        ui->lastEventsTableWidget->verticalHeader()->resizeSections(QHeaderView::ResizeToContents);
        //

        m_notificationWindow = new NotificationWindow(QString("Документ №%1 отклонен! ")
                                                      .arg(m_jsonParse.getJsonObjectData(jsonData, "documentID").toString().toInt()),
                                                      NULL);
        connect(m_notificationWindow, SIGNAL(closeWindow()),
                m_notificationWindow, SLOT(deleteLater()));
        m_notificationWindow->show();
    }
}

void MainWindow::onAuthorDocumentRejected(const QString &jsonData)
{
    if (m_jsonParse.getMethodName(jsonData) == "authorDocumentRejected" &&
            m_jsonParse.getServerErrorCode(jsonData) == 0) {

        //Добавление информации в список последних событий
        m_lastEventsCount++;
        ui->lastEventsTableWidget->insertRow(0);
        ui->lastEventsTableWidget->setItem(0,
                                           0,
                                           new QTableWidgetItem(QString::number(m_lastEventsCount)));
        ui->lastEventsTableWidget->setItem(0,
                                           1,
                                           new QTableWidgetItem(QString("Документ №%1 был отклонен автором! ")
                                                                .arg(m_jsonParse.getJsonObjectData(jsonData, "documentID").toString().toInt())));
        ui->lastEventsTableWidget->setItem(0,
                                           2,
                                           new QTableWidgetItem(QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss")));

        ui->lastEventsTableWidget->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
        ui->lastEventsTableWidget->verticalHeader()->resizeSections(QHeaderView::ResizeToContents);
        //

        m_notificationWindow = new NotificationWindow(QString("Документ №%1 был отклонен автором! ")
                                                      .arg(m_jsonParse.getJsonObjectData(jsonData, "documentID").toString().toInt()),
                                                      NULL);
        connect(m_notificationWindow, SIGNAL(closeWindow()),
                m_notificationWindow, SLOT(deleteLater()));
        m_notificationWindow->show();
    }
}

void MainWindow::onNewDocumentCommented(const QString &jsonData)
{
    if (m_jsonParse.getMethodName(jsonData) == "newDocumentCommented" &&
            m_jsonParse.getServerErrorCode(jsonData) == 0) {

        //Добавление информации в список последних событий
        m_lastEventsCount++;
        ui->lastEventsTableWidget->insertRow(0);
        ui->lastEventsTableWidget->setItem(0,
                                           0,
                                           new QTableWidgetItem(QString::number(m_lastEventsCount)));
        ui->lastEventsTableWidget->setItem(0,
                                           1,
                                           new QTableWidgetItem(QString("Уточнение по документу №%1 ")
                                                                .arg(m_jsonParse.getJsonObjectData(jsonData, "documentID").toString().toInt())));
        ui->lastEventsTableWidget->setItem(0,
                                           2,
                                           new QTableWidgetItem(QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss")));

        ui->lastEventsTableWidget->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
        ui->lastEventsTableWidget->verticalHeader()->resizeSections(QHeaderView::ResizeToContents);
        //

        m_notificationWindow = new NotificationWindow(QString("Уточнение по документу №%1 ")
                                                      .arg(m_jsonParse.getJsonObjectData(jsonData, "documentID").toString().toInt()),
                                                      NULL);
        connect(m_notificationWindow, SIGNAL(closeWindow()), m_notificationWindow, SLOT(deleteLater()));
        m_notificationWindow->show();
    }
}

void MainWindow::onAuthorDocumentCommented(const QString &jsonData)
{
    if (m_jsonParse.getMethodName(jsonData) == "authorDocumentCommented" &&
            m_jsonParse.getServerErrorCode(jsonData) == 0) {

        //Добавление информации в список последних событий
        m_lastEventsCount++;
        ui->lastEventsTableWidget->insertRow(0);
        ui->lastEventsTableWidget->setItem(0,
                                           0,
                                           new QTableWidgetItem(QString::number(m_lastEventsCount)));
        ui->lastEventsTableWidget->setItem(0,
                                           1,
                                           new QTableWidgetItem(QString("Уточнение по документу №%1 от автора ")
                                                                .arg(m_jsonParse.getJsonObjectData(jsonData, "documentID").toString().toInt())));
        ui->lastEventsTableWidget->setItem(0,
                                           2,
                                           new QTableWidgetItem(QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss")));

        ui->lastEventsTableWidget->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
        ui->lastEventsTableWidget->verticalHeader()->resizeSections(QHeaderView::ResizeToContents);
        //

        m_notificationWindow = new NotificationWindow(QString("Уточнение по документу №%1 от автора ")
                                                      .arg(m_jsonParse.getJsonObjectData(jsonData, "documentID").toString().toInt()),
                                                      NULL);
        connect(m_notificationWindow, SIGNAL(closeWindow()),
                m_notificationWindow, SLOT(deleteLater()));
        m_notificationWindow->show();
    }
}

void MainWindow::onNewDocumentUploaded(const QString &jsonData)
{
    if (m_jsonParse.getMethodName(jsonData) == "newDocumentUploaded" &&
            m_jsonParse.getServerErrorCode(jsonData) == 0) {

        //Добавление информации в список последних событий
        m_lastEventsCount++;
        ui->lastEventsTableWidget->insertRow(0);
        ui->lastEventsTableWidget->setItem(0,
                                           0,
                                           new QTableWidgetItem(QString::number(m_lastEventsCount)));
        ui->lastEventsTableWidget->setItem(0,
                                           1,
                                           new QTableWidgetItem(QString("Автором загружен новый текст документа по документу №%1 ")
                                                                .arg(m_jsonParse.getJsonObjectData(jsonData, "documentID").toString().toInt())));
        ui->lastEventsTableWidget->setItem(0,
                                           2,
                                           new QTableWidgetItem(QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss")));

        ui->lastEventsTableWidget->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
        ui->lastEventsTableWidget->verticalHeader()->resizeSections(QHeaderView::ResizeToContents);
        //

        m_notificationWindow = new NotificationWindow(QString("Автором загружен новый текст документа по документу №%1 ")
                                                      .arg(m_jsonParse.getJsonObjectData(jsonData, "documentID").toString().toInt()),
                                                      NULL);
        connect(m_notificationWindow, SIGNAL(closeWindow()),
                m_notificationWindow, SLOT(deleteLater()));
        m_notificationWindow->show();
    }
}

void MainWindow::onNewEventsNotificationRecieved(const QString &jsonData)
{
    if (jsonData.indexOf("newEventsNotification", Qt::CaseInsensitive) != -1) {
        QString resultData = jsonData;
        resultData = resultData.replace("newEventsNotification:", "");
        QJsonDocument jsonDoc = QJsonDocument::fromJson(resultData.toUtf8());

        if (jsonDoc.isArray()) {
            QJsonArray jsonArray = jsonDoc.array();

            for(int i = 0; i < jsonArray.count(); i++) {
                QJsonValue v = jsonArray.at(i);
                QJsonObject o = v.toObject();

                m_lastEventsCount++;
                ui->lastEventsTableWidget->insertRow(0);
                ui->lastEventsTableWidget->setItem(0,
                                                   0,
                                                   new QTableWidgetItem(QString::number(m_lastEventsCount)));

                if (o.value("methodName").toString() == "newDocumentRecieved") {
                    ui->lastEventsTableWidget->setItem(0,
                                                       1,
                                                       new QTableWidgetItem("Получен новый документ для рассмотрения "));
                }

                if (o.value("methodName").toString() == "newDocumentAccepted") {
                    ui->lastEventsTableWidget->setItem(0,
                                                       1,
                                                       new QTableWidgetItem(QString("Подтверждение по документу %1 ")
                                                                            .arg(o.value("documentID").toInt())));
                }

                if (o.value("methodName").toString() == "newDocumentRejected") {
                    ui->lastEventsTableWidget->setItem(0,
                                                       1,
                                                       new QTableWidgetItem(QString("Документ №%1 отклонен! ")
                                                                            .arg(o.value("documentID").toInt())));
                }

                if (o.value("methodName").toString() == "authorDocumentRejected") {
                    ui->lastEventsTableWidget->setItem(0,
                                                       1,
                                                       new QTableWidgetItem(QString("Документ №%1 был отклонен автором! ")
                                                                            .arg(o.value("documentID").toInt())));
                }

                if (o.value("methodName").toString() == "newDocumentCommented") {
                    ui->lastEventsTableWidget->setItem(0,
                                                       1,
                                                       new QTableWidgetItem(QString("Уточнение по документу №%1 ")
                                                                            .arg(o.value("documentID").toInt())));
                }

                if (o.value("methodName").toString() == "authorDocumentCommented") {
                    ui->lastEventsTableWidget->setItem(0,
                                                       1,
                                                       new QTableWidgetItem(QString("Уточнение по документу №%1 от автора ")
                                                                            .arg(o.value("documentID").toInt())));
                }

                if (o.value("methodName").toString() == "newDocumentUploaded") {
                    ui->lastEventsTableWidget->setItem(0,
                                                       1,
                                                       new QTableWidgetItem(QString("Автором загружен новый текст документа по документу №%1 ")
                                                                            .arg(o.value("documentID").toInt())));
                }

                if (o.value("methodName").toString() == "newDocumentUploaded") {
                    ui->lastEventsTableWidget->setItem(0,
                                                       1,
                                                       new QTableWidgetItem(QString("Автором загружен новый текст документа по документу №%1 ")
                                                                            .arg(o.value("documentID").toInt())));
                }

                ui->lastEventsTableWidget->setItem(0,
                                                   2,
                                                   new QTableWidgetItem(QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss")));
            }

            ui->lastEventsTableWidget->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
            ui->lastEventsTableWidget->verticalHeader()->resizeSections(QHeaderView::ResizeToContents);
        }

        m_notificationWindow = new NotificationWindow("Произошли новые события! ",
                                                      NULL);
        connect(m_notificationWindow, SIGNAL(closeWindow()),
                m_notificationWindow, SLOT(deleteLater()));
        m_notificationWindow->show();
    }
}

void MainWindow::on_exitAction_triggered()
{
    qApp->quit();
}

void MainWindow::on_createNewDocumentAction_triggered()
{
    m_cndw = new CreateNewDocumentWindow(NULL);
    connect(m_cndw, SIGNAL(closeWindow()),
            m_cndw, SLOT(deleteLater()));
    m_cndw->show();
}

void MainWindow::on_myDocumentsAction_triggered()
{
    m_mdw = new MyDocumentsWindow(NULL);
    connect(m_mdw, SIGNAL(closeWindow()),
            m_mdw, SLOT(deleteLater()));
    m_mdw->show();
}

void MainWindow::on_myDocumentsSignatureAction_triggered()
{
    m_mdsw = new MyDocumentsSignatureWindow(NULL);
    connect(m_mdsw, SIGNAL(closeWindow()),
            m_mdsw, SLOT(deleteLater()));
    m_mdsw->show();
}

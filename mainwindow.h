#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>
#include <QDebug>
#include <QSystemTrayIcon>
#include <QTableWidgetItem>
#include <QDateTime>

#include "coreclass.h"
#include "createnewdocumentwindow.h"
#include "jsonParse/jsonparse.h"
#include "notificationwindow.h"
#include "mydocumentswindow.h"
#include "mydocumentssignaturewindow.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void closeEvent(QCloseEvent *event);

private slots:
    void onClientConnectedToServer(const bool &result);

    void onUserQuited(const QString &jsonData);
    void onNewDocumentRecieved(const QString &jsonData);
    void onNewDocumentAccepted(const QString &jsonData);
    void onNewDocumentRejected(const QString &jsonData);
    void onAuthorDocumentRejected(const QString &jsonData);
    void onNewDocumentCommented(const QString &jsonData);
    void onAuthorDocumentCommented(const QString &jsonData);
    void onNewDocumentUploaded(const QString &jsonData);
    void onNewEventsNotificationRecieved(const QString &jsonData);


    void on_exitAction_triggered();
    void on_createNewDocumentAction_triggered();

    void iconActivated(QSystemTrayIcon::ActivationReason reason);
    void closeApplication();

    void on_myDocumentsAction_triggered();
    void on_myDocumentsSignatureAction_triggered();

private:
    Ui::MainWindow *ui;
    CreateNewDocumentWindow *m_cndw;
    JsonParse m_jsonParse;
    QSystemTrayIcon *m_trayIcon;
    NotificationWindow *m_notificationWindow;
    MyDocumentsWindow *m_mdw;
    MyDocumentsSignatureWindow *m_mdsw;
    int m_lastEventsCount;
};
#endif // MAINWINDOW_H

#include "mydocumentssignaturewindow.h"
#include "ui_mydocumentssignaturewindow.h"

#define m_tcpClient CoreClass::instance()->tcpClient()
#define m_appStorage CoreClass::instance()->appStorage()

MyDocumentsSignatureWindow::MyDocumentsSignatureWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MyDocumentsSignatureWindow)
{
    ui->setupUi(this);

    connect(m_tcpClient, SIGNAL(getMyDocumentSignatureListFinished(QString)),
            this, SLOT(onGetMyDocumentSignatureListFinished(QString)));
    m_tcpClient->getMyDocumentSignatureList(m_appStorage->getUserToken());
}

MyDocumentsSignatureWindow::~MyDocumentsSignatureWindow()
{
    delete ui;
}

void MyDocumentsSignatureWindow::closeEvent(QCloseEvent *event)
{
    emit closeWindow();
}

void MyDocumentsSignatureWindow::onGetMyDocumentSignatureListFinished(const QString &jsonData)
{
    if (jsonData.indexOf("getMyDocumentSignatureList", Qt::CaseInsensitive) != -1) {
        QString resultData = jsonData;
        resultData = resultData.replace("getMyDocumentSignatureList:", "");
        QJsonDocument jsonDoc = QJsonDocument::fromJson(resultData.toUtf8());

        if (jsonDoc.isArray()) {
            QJsonArray jsonArray = jsonDoc.array();

            ui->myDocumentsTableWidget->setRowCount(jsonArray.count());
            ui->myDocumentsTableWidget->setColumnCount(10);
            ui->myDocumentsTableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
            ui->myDocumentsTableWidget->setColumnHidden(3, true);
            ui->myDocumentsTableWidget->setColumnHidden(7, true);
            ui->myDocumentsTableWidget->setColumnHidden(8, true);

            QStringList tableHeader;
            tableHeader << "Номер документа" << "Название" << "Краткое описание" <<
                           "Код статуса" << "Статус" << "Дата/время создания" <<
                           "Создатель" << "Код создателя" << "Код стадии" <<
                           "Мое решение";
            ui->myDocumentsTableWidget->setHorizontalHeaderLabels(tableHeader);

            for(int i = 0; i < jsonArray.count(); i++) {
                QJsonObject o = jsonArray.at(i).toObject();

                ui->myDocumentsTableWidget->setItem(i,
                                                    0,
                                                    new QTableWidgetItem(o.value("textDocumentID").toString()));
                ui->myDocumentsTableWidget->setItem(i,
                                                    1,
                                                    new QTableWidgetItem(o.value("textDocumentName").toString()));
                ui->myDocumentsTableWidget->setItem(i,
                                                    2,
                                                    new QTableWidgetItem(o.value("textDocumentShortDescription").toString()));
                ui->myDocumentsTableWidget->setItem(i,
                                                    3,
                                                    new QTableWidgetItem(o.value("textDocumentStatusID").toString()));
                ui->myDocumentsTableWidget->setItem(i,
                                                    4,
                                                    new QTableWidgetItem(o.value("textDocumentStatusName").toString()));
                ui->myDocumentsTableWidget->setItem(i,
                                                    5,
                                                    new QTableWidgetItem(m_dateTime.getHumanDateTimeText(o.value("textDocumentDateTime").toString())));
                ui->myDocumentsTableWidget->setItem(i,
                                                    6,
                                                    new QTableWidgetItem(QString("%1 %2")
                                                                         .arg(o.value("userName").toString())
                                                                         .arg(o.value("userSurname").toString())));
                ui->myDocumentsTableWidget->setItem(i,
                                                    7,
                                                    new QTableWidgetItem(o.value("userTextDocumentOfferUserID").toString()));
                ui->myDocumentsTableWidget->setItem(i,
                                                    8,
                                                    new QTableWidgetItem(o.value("textDocumentStageLastStatusID").toString()));
                ui->myDocumentsTableWidget->setItem(i,
                                                    9,
                                                    new QTableWidgetItem(o.value("textDocumentStageStatusName").toString()));
            }
        }
        else {
            QMessageBox::information(this,
                                     "EDMS CLient",
                                     "Ошибка получения списка моих документов! ");
        }
    }
    else {
        QMessageBox::information(this,
                                 "EDMS CLient",
                                 "Ошибка получения списка моих документов! ");
    }
}

void MyDocumentsSignatureWindow::on_myDocumentsTableWidget_cellDoubleClicked(int row, int column)
{
    m_currentTableRow = row;
    int documentID = ui->myDocumentsTableWidget->item(row, 0)->text().toInt();
    m_dscw = new DocumentSignatureCardWindow(documentID,
                                             ui->myDocumentsTableWidget->item(row, 1)->text(),
                                             ui->myDocumentsTableWidget->item(row, 2)->text(),
                                             ui->myDocumentsTableWidget->item(row, 3)->text().toInt(),
                                             ui->myDocumentsTableWidget->item(row, 4)->text(),
                                             ui->myDocumentsTableWidget->item(row, 5)->text(),
                                             ui->myDocumentsTableWidget->item(row, 6)->text(),
                                             ui->myDocumentsTableWidget->item(row, 7)->text().toInt(),
                                             ui->myDocumentsTableWidget->item(row, 8)->text().toInt(),
                                             NULL);
    connect(m_dscw, SIGNAL(closeWindow()),
            m_dscw, SLOT(deleteLater()));
    connect(m_dscw, SIGNAL(documentAccepted(int)),
            this, SLOT(onDocumentAccepted(int)));
    connect(m_dscw, SIGNAL(documentRejected(int)),
            this, SLOT(onDocumentRejected(int)));
    connect(m_dscw, SIGNAL(documentCommented(int)),
            this, SLOT(onDocumentCommented(int)));
    connect(m_dscw, SIGNAL(documentAccepted(int)),
            m_dscw, SLOT(deleteLater()));
    connect(m_dscw, SIGNAL(documentRejected(int)),
            m_dscw, SLOT(deleteLater()));
    connect(m_dscw, SIGNAL(documentCommented(int)),
            m_dscw, SLOT(deleteLater()));
    m_dscw->show();
}

void MyDocumentsSignatureWindow::onDocumentAccepted(const int &documentID)
{
    if (m_currentTableRow != 0) {
        ui->myDocumentsTableWidget->item(m_currentTableRow, 8)->setText(QString::number(1));
        ui->myDocumentsTableWidget->item(m_currentTableRow, 9)->setText("Принято");
    }
    else {
        for(int i = 0; i < ui->myDocumentsTableWidget->rowCount(); i++) {
            if (ui->myDocumentsTableWidget->item(i, 0)->text().toInt() == documentID) {
                ui->myDocumentsTableWidget->item(i, 8)->setText(QString::number(1));
                ui->myDocumentsTableWidget->item(i, 9)->setText("Принято");
                break;
            }
        }
    }
}

void MyDocumentsSignatureWindow::onDocumentRejected(const int &documentID)
{
    if (m_currentTableRow != 0) {
        ui->myDocumentsTableWidget->item(m_currentTableRow, 3)->setText(QString::number(4));
        ui->myDocumentsTableWidget->item(m_currentTableRow, 4)->setText("Отклонен");
        ui->myDocumentsTableWidget->item(m_currentTableRow, 8)->setText(QString::number(2));
        ui->myDocumentsTableWidget->item(m_currentTableRow, 9)->setText("Отклонено");
    }
    else {
        for(int i = 0; i < ui->myDocumentsTableWidget->rowCount(); i++) {
            if (ui->myDocumentsTableWidget->item(i, 0)->text().toInt() == documentID) {
                ui->myDocumentsTableWidget->item(i, 3)->setText(QString::number(4));
                ui->myDocumentsTableWidget->item(i, 4)->setText("Отклонен");
                ui->myDocumentsTableWidget->item(i, 8)->setText(QString::number(2));
                ui->myDocumentsTableWidget->item(i, 9)->setText("Отклонено");
                break;
            }
        }
    }
}

void MyDocumentsSignatureWindow::onDocumentCommented(const int &documentID)
{
    if (m_currentTableRow != 0) {
        ui->myDocumentsTableWidget->item(m_currentTableRow, 8)->setText(QString::number(4));
        ui->myDocumentsTableWidget->item(m_currentTableRow, 9)->setText("Уточнение");
    }
    else {
        for(int i = 0; i < ui->myDocumentsTableWidget->rowCount(); i++) {
            if (ui->myDocumentsTableWidget->item(i, 0)->text().toInt() == documentID) {
                ui->myDocumentsTableWidget->item(i, 8)->setText(QString::number(4));
                ui->myDocumentsTableWidget->item(i, 9)->setText("Уточнение");
                break;
            }
        }
    }
}

#ifndef MYDOCUMENTSSIGNATUREWINDOW_H
#define MYDOCUMENTSSIGNATUREWINDOW_H

#include <QDialog>

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonObject>
#include <QVariant>

#include <QMessageBox>
#include <QTableWidgetItem>

#include "coreclass.h"
#include "documentsignaturecardwindow.h"
#include "datetime/datetime.h"

namespace Ui {
class MyDocumentsSignatureWindow;
}

class MyDocumentsSignatureWindow : public QDialog
{
    Q_OBJECT

public:
    explicit MyDocumentsSignatureWindow(QWidget *parent = nullptr);
    ~MyDocumentsSignatureWindow();

    void closeEvent(QCloseEvent *event);

private slots:
    void onGetMyDocumentSignatureListFinished(const QString &jsonData);
    void onDocumentAccepted(const int &documentID);
    void onDocumentRejected(const int &documentID);
    void onDocumentCommented(const int &documentID);

    void on_myDocumentsTableWidget_cellDoubleClicked(int row, int column);

signals:
    void closeWindow();

private:
    Ui::MyDocumentsSignatureWindow *ui;
    DocumentSignatureCardWindow *m_dscw;
    DateTime m_dateTime;
    int m_currentTableRow;
};

#endif // MYDOCUMENTSSIGNATUREWINDOW_H

#include "mydocumentswindow.h"
#include "ui_mydocumentswindow.h"

#define m_tcpClient CoreClass::instance()->tcpClient()
#define m_appStorage CoreClass::instance()->appStorage()

MyDocumentsWindow::MyDocumentsWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MyDocumentsWindow)
{
    ui->setupUi(this);

    m_currentTableRow = 0;
    connect(m_tcpClient, SIGNAL(getMyDocumentListFinished(QString)),
            this, SLOT(onGetMyDocumentListFinished(QString)));
    m_tcpClient->getMyDocumentList(m_appStorage->getUserToken());
}

MyDocumentsWindow::~MyDocumentsWindow()
{
    delete ui;
}

void MyDocumentsWindow::closeEvent(QCloseEvent *event)
{
    emit closeWindow();
}

void MyDocumentsWindow::onGetMyDocumentListFinished(const QString &jsonData)
{
//    qDebug() << jsonData;

    if (jsonData.indexOf("getMyDocumentList", Qt::CaseInsensitive) != -1) {
        QString resultData = jsonData;
        resultData = resultData.replace("getMyDocumentList:", "");
        QJsonDocument jsonDoc = QJsonDocument::fromJson(resultData.toUtf8());

        if (jsonDoc.isArray()) {
            QJsonArray jsonArray = jsonDoc.array();

            ui->myDocumentsTableWidget->setRowCount(jsonArray.count());
            ui->myDocumentsTableWidget->setColumnCount(6);
            ui->myDocumentsTableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
            ui->myDocumentsTableWidget->setColumnHidden(3, true);

            QStringList tableHeader;
            tableHeader << "Номер документа" << "Название" << "Краткое описание" << "Код статуса" << "Статус" << "Дата/время создания";
            ui->myDocumentsTableWidget->setHorizontalHeaderLabels(tableHeader);

            for(int i = 0; i < jsonArray.count(); i++) {
                QJsonObject o = jsonArray.at(i).toObject();

                ui->myDocumentsTableWidget->setItem(i,
                                                    0,
                                                    new QTableWidgetItem(o.value("textDocumentID").toString()));
                ui->myDocumentsTableWidget->setItem(i,
                                                    1,
                                                    new QTableWidgetItem(o.value("textDocumentName").toString()));
                ui->myDocumentsTableWidget->setItem(i,
                                                    2,
                                                    new QTableWidgetItem(o.value("textDocumentShortDescription").toString()));
                ui->myDocumentsTableWidget->setItem(i,
                                                    3,
                                                    new QTableWidgetItem(QString::number(o.value("textDocumentStatusID").toInt())));
                ui->myDocumentsTableWidget->setItem(i,
                                                    4,
                                                    new QTableWidgetItem(o.value("textDocumentStatusName").toString()));
                ui->myDocumentsTableWidget->setItem(i,
                                                    5,
                                                    new QTableWidgetItem(m_dateTime.getHumanDateTimeText(o.value("textDocumentDateTime").toString())));
            }
        }
        else {
            QMessageBox::information(this,
                                     "EDMS CLient",
                                     "Ошибка получения списка моих документов! ");
        }
    }
    else {
        QMessageBox::information(this,
                                 "EDMS CLient",
                                 "Ошибка получения списка моих документов! ");
    }
}

void MyDocumentsWindow::on_myDocumentsTableWidget_cellDoubleClicked(int row, int column)
{
    m_currentTableRow = row;
    int documentID = ui->myDocumentsTableWidget->item(row, 0)->text().toInt();
    m_dcw = new DocumentCardWindow(documentID,
                                   ui->myDocumentsTableWidget->item(row, 1)->text(),
                                   ui->myDocumentsTableWidget->item(row, 2)->text(),
                                   ui->myDocumentsTableWidget->item(row, 3)->text().toInt(),
                                   ui->myDocumentsTableWidget->item(row, 4)->text(),
                                   ui->myDocumentsTableWidget->item(row, 5)->text(),
                                   this);
    connect(m_dcw, SIGNAL(closeWindow()),
            m_dcw, SLOT(deleteLater()));
    connect(m_dcw, SIGNAL(documentRejected(int)),
            this, SLOT(onDocumentRejected(int)));
    connect(m_dcw, SIGNAL(documentRejected(int)),
            m_dcw, SLOT(deleteLater()));
    m_dcw->show();
}

void MyDocumentsWindow::onDocumentRejected(const int &documentID)
{
    if (m_currentTableRow != 0) {
        ui->myDocumentsTableWidget->item(m_currentTableRow, 3)->setText(QString::number(4));
        ui->myDocumentsTableWidget->item(m_currentTableRow, 4)->setText("Отклонен");
    }
    else {
        for(int i = 0; i < ui->myDocumentsTableWidget->rowCount(); i++) {
            if (ui->myDocumentsTableWidget->item(i, 0)->text().toInt() == documentID) {
                ui->myDocumentsTableWidget->item(i, 3)->setText(QString::number(4));
                ui->myDocumentsTableWidget->item(i, 4)->setText("Отклонен");
                break;
            }
        }
    }
}

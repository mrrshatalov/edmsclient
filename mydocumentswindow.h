#ifndef MYDOCUMENTSWINDOW_H
#define MYDOCUMENTSWINDOW_H

#include <QDialog>

#include <QDebug>

#include "coreclass.h"
#include <QMessageBox>
#include <QTableWidgetItem>

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonObject>
#include <QVariant>

#include "documentcardwindow.h"
#include "datetime/datetime.h"

namespace Ui {
class MyDocumentsWindow;
}

class MyDocumentsWindow : public QDialog
{
    Q_OBJECT

public:
    explicit MyDocumentsWindow(QWidget *parent = nullptr);
    ~MyDocumentsWindow();

    void closeEvent(QCloseEvent *event);

private slots:
    void onGetMyDocumentListFinished(const QString &jsonData);
    void onDocumentRejected(const int &documentID);

    void on_myDocumentsTableWidget_cellDoubleClicked(int row, int column);

signals:
    void closeWindow();

private:
    Ui::MyDocumentsWindow *ui;
    QTableWidgetItem *m_tableWidgetItem;
    DocumentCardWindow *m_dcw;
    DateTime m_dateTime;
    int m_currentTableRow;
};

#endif // MYDOCUMENTSWINDOW_H

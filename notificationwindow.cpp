#include "notificationwindow.h"
#include "ui_notificationwindow.h"

NotificationWindow::NotificationWindow(const QString &notificateionText,
                                       QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NotificationWindow)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);

    if (!notificateionText.isEmpty()) {
        ui->headerLabel->setText(notificateionText);
        ui->headerLabel->setWordWrap(true);
    }

    //Позиционирование окна
    QDesktopWidget dw;
    this->setGeometry(dw.availableGeometry().width() - this->width() - 20,
                      dw.availableGeometry().height() - this->height() - 20,
                      290, 85);
    //
}

NotificationWindow::~NotificationWindow()
{
    delete ui;
}

void NotificationWindow::on_pushButton_clicked()
{
    emit closeWindow();
}

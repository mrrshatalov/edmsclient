#ifndef NOTIFICATIONWINDOW_H
#define NOTIFICATIONWINDOW_H

#include <QDialog>

#include <QDesktopWidget>

namespace Ui {
class NotificationWindow;
}

class NotificationWindow : public QDialog
{
    Q_OBJECT

public:
    explicit NotificationWindow(const QString &notificateionText,
                                QWidget *parent = nullptr);
    ~NotificationWindow();

private slots:
    void on_pushButton_clicked();

signals:
    void closeWindow();

private:
    Ui::NotificationWindow *ui;
};

#endif // NOTIFICATIONWINDOW_H

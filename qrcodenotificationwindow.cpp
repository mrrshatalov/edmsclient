#include "qrcodenotificationwindow.h"
#include "ui_qrcodenotificationwindow.h"

QrCodeNotificationWindow::QrCodeNotificationWindow(const QString &qrCodeData,
                                                   const int &documentID,
                                                   QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QrCodeNotificationWindow)
{
    setData(qrCodeData,
            documentID);
    ui->setupUi(this);

    showQrCode();
}

QrCodeNotificationWindow::~QrCodeNotificationWindow()
{
    delete ui;
}

void QrCodeNotificationWindow::closeEvent(QCloseEvent *event)
{
    emit closeWindow();
}

void QrCodeNotificationWindow::setData(const QString &qrCodeData,
                                       const int &documentID)
{
    m_qrCodeData = qrCodeData;
    m_documentID = documentID;
}

void QrCodeNotificationWindow::showQrCode()
{
    QByteArray b;
    b.clear();
    b.append(m_qrCodeData);
    b = QByteArray::fromBase64(b);

    m_qrCodeImage = QImage::fromData(b, "PNG");
    ui->qrCodeImageLabel->setPixmap(QPixmap::fromImage(m_qrCodeImage));
}

void QrCodeNotificationWindow::on_saveQrCodeButton_clicked()
{
    QString imagePath = QFileDialog::getExistingDirectory(this,
                                                          "EDMS Client",
                                                          "");
    if (!imagePath.isEmpty()) {
        if (m_qrCodeImage.save(QString("%1/document_%2.png")
                           .arg(imagePath)
                           .arg(m_documentID),
                           "PNG",
                           100)) {
            QMessageBox::information(this,
                                     "EDMS CLient",
                                     "Qr-код успешно сохранен! ");
            emit closeWindow();
        }
        else {
            QMessageBox::information(this,
                                     "EDMS CLient",
                                     "Произошла ошибка при сохранении Qr-кода! ");
        }
    }
}

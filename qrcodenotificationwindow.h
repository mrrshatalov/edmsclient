#ifndef QRCODENOTIFICATIONWINDOW_H
#define QRCODENOTIFICATIONWINDOW_H

#include <QDialog>

#include <QMessageBox>
#include <QByteArray>
#include <QImage>
#include <QPixmap>
#include <QFileDialog>

namespace Ui {
class QrCodeNotificationWindow;
}

class QrCodeNotificationWindow : public QDialog
{
    Q_OBJECT

public:
    explicit QrCodeNotificationWindow(const QString &qrCodeData,
                                      const int &documentID,
                                      QWidget *parent = nullptr);
    ~QrCodeNotificationWindow();
    void closeEvent(QCloseEvent *event);

signals:
    void closeWindow();

private slots:
    void on_saveQrCodeButton_clicked();

private:
    Ui::QrCodeNotificationWindow *ui;

    QString m_qrCodeData;
    int m_documentID;
    void setData(const QString &qrCodeData,
                 const int &documentID);
    void showQrCode();
    QImage m_qrCodeImage;
};

#endif // QRCODENOTIFICATIONWINDOW_H

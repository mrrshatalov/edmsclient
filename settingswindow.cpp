#include "settingswindow.h"
#include "ui_settingswindow.h"

SettingsWindow::SettingsWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsWindow)
{
    ui->setupUi(this);

    m_settings = new QSettings(QString("%1/settings.ini")
                               .arg(qApp->applicationDirPath()),
                               QSettings::IniFormat);
    getApplicationSettings();
}

SettingsWindow::~SettingsWindow()
{
    m_settings->deleteLater();

    delete ui;
}

void SettingsWindow::closeEvent(QCloseEvent *event)
{
    emit closeWindow();
}

void SettingsWindow::getApplicationSettings()
{
    m_settings->sync();

    if (m_settings->contains("serverAddress")) {
        ui->serverAddressLine->setText(m_settings->value("serverAddress").toString());
    }

    if (m_settings->contains("serverPort")) {
        ui->serverPortLine->setText(m_settings->value("serverPort").toString());
    }
}

void SettingsWindow::on_saveButton_clicked()
{
    QString serverAddress;
    int serverPort = 0;
    bool z_ok = true;

    serverAddress = ui->serverAddressLine->text();
    serverPort = ui->serverPortLine->text().toInt(&z_ok);

    if (serverAddress.isEmpty() ||
            ui->serverPortLine->text().isEmpty()) {
        if (serverAddress.isEmpty()) {
            QMessageBox::information(this,
                                     "EDMS Client",
                                     "Введите адрес сервера! ");
        }

        if (ui->serverPortLine->text().isEmpty()) {
            QMessageBox::information(this,
                                     "EDMS Client",
                                     "Введите порт сервера! ");
        }
    }
    else {
        if (!z_ok) {
            QMessageBox::information(this,
                                     "EDMS Client",
                                     "Порт сервера должен быть числом! ");
        }
        else {
            if (serverPort < 0 || serverPort == 0) {
                QMessageBox::information(this,
                                         "EDMS Client",
                                         "Порт сервера не может быть равен нулю или иметь отрицательное значение! ");
            }
            else {
                m_settings->setValue("serverAddress", serverAddress);
                m_settings->setValue("serverPort", QString::number(serverPort));
                m_settings->sync();

                emit closeWindow();
            }
        }
    }
}

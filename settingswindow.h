#ifndef SETTINGSWINDOW_H
#define SETTINGSWINDOW_H

#include <QDialog>

#include <QSettings>
#include <QMessageBox>

namespace Ui {
class SettingsWindow;
}

class SettingsWindow : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsWindow(QWidget *parent = nullptr);
    ~SettingsWindow();

    void closeEvent(QCloseEvent *event);

signals:
    void closeWindow();

private slots:
    void on_saveButton_clicked();

private:
    Ui::SettingsWindow *ui;
    void getApplicationSettings();
    QSettings *m_settings;
};

#endif // SETTINGSWINDOW_H

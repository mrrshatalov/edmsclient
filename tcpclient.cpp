#include "tcpclient.h"

TCPClient::TCPClient(QObject *parent) : QObject(parent)
{
    m_result.clear();
    m_serverAddress.clear();
    m_serverPort = 0;

    m_client = new QTcpSocket(this);
    connect(m_client, SIGNAL(stateChanged(QAbstractSocket::SocketState)),
            this, SLOT(onClientStateChanged(QAbstractSocket::SocketState)));
    connect(m_client, SIGNAL(readyRead()),
            this, SLOT(onClientReadyRead()));
}

TCPClient::~TCPClient()
{
    m_result.clear();
    m_serverAddress.clear();
    m_serverPort = 0;

    m_client->disconnectFromHost();
    m_client->deleteLater();
}

QString TCPClient::setBlockQuerySymbols(QString data)
{
    return data.append("}").prepend("{");
}

void TCPClient::startTCPClient(const QString &serverAddress,
                               const quint16 &serverPort)
{
    m_serverAddress = serverAddress;
    m_serverPort = serverPort;
//    qDebug() << "Подключение к " << serverAddress;

    m_client->connectToHost(serverAddress, serverPort);
    if (m_client->waitForConnected(5 * 1000)) {
//        qDebug() << "Успешное подключение к серверу! ";
        emit clientConnectedToServer(true);
    }
    else {
        emit clientConnectedToServer(false);
    }
}

void TCPClient::onClientStateChanged(QAbstractSocket::SocketState socketState)
{
//    qDebug() << "client state - " << socketState;

    if (socketState == QAbstractSocket::SocketState::UnconnectedState) {
        emit clientConnectedToServer(false);
        QTimer::singleShot(2 * 1000,
                           this,
                           SLOT(startClientSlot()));
    }
}

void TCPClient::startClientSlot()
{
//    qDebug() << "Подключаемся к " << m_serverAddress << m_serverPort;

    m_client->connectToHost(m_serverAddress, m_serverPort);
    if (m_client->waitForConnected(5 * 1000)) {
//        qDebug() << "Успешное переподключение к серверу! ";
        emit clientConnectedToServer(true);
    }
    else {
        emit clientConnectedToServer(false);
    }
}

void TCPClient::onClientReadyRead()
{
//    qDebug() << "m_client->bytesAvailable() - " << m_client->size() << m_client->bytesAvailable();

    while (!m_client->atEnd()) {
        if (m_client->bytesAvailable() > 0) {
            m_result.append(QString::fromLocal8Bit(m_client->readAll()));
        }
    }

    bool isAllDataRecieved = false;
    if (m_result.indexOf("[", Qt::CaseInsensitive) != -1) {
        if (m_result.mid(m_result.length() - 1, 1) == "]") {
            isAllDataRecieved = true;
        }
    }
    else {
        if (m_result.indexOf("{", Qt::CaseInsensitive) != -1) {
            if (m_result.mid(m_result.length() - 1, 1) == "}") {
                isAllDataRecieved = true;
            }
        }
    }

    if (isAllDataRecieved) {
//        qDebug() << m_result;

        if (m_result.indexOf("userEnter", Qt::CaseInsensitive) != -1) {
            emit userEntered(m_result);
        }

        if (m_result.indexOf("userQuit", Qt::CaseInsensitive) != -1) {
            emit userQuited(m_result);
        }

        if (m_result.indexOf("getUserList", Qt::CaseInsensitive) != -1) {
            emit getUserListFinished(m_result);
        }

        if (m_result.indexOf("createNewDocument", Qt::CaseInsensitive) != -1) {
            emit createNewDocumentFinished(m_result);
        }

        if (m_result.indexOf("getMyDocumentList", Qt::CaseInsensitive) != -1) {
            emit getMyDocumentListFinished(m_result);
        }

        if (m_result.indexOf("getMyDocumentSignatureList", Qt::CaseInsensitive) != -1) {
            emit getMyDocumentSignatureListFinished(m_result);
        }

        if (m_result.indexOf("rejectDocument", Qt::CaseInsensitive) != -1) {
            emit rejectDocumentFinished(m_result);
        }

        if (m_result.indexOf("authorRejectDocument", Qt::CaseInsensitive) != -1) {
            emit authorRejectDocumentFinished(m_result);
        }

        if (m_result.indexOf("getDocumentHistory", Qt::CaseInsensitive) != -1) {
            emit getDocumentHistoryFinished(m_result);
        }

        if (m_result.indexOf("commentDocument", Qt::CaseInsensitive) != -1) {
            emit commentDocumentFinished(m_result);
        }

        if (m_result.indexOf("authorCommentDocument", Qt::CaseInsensitive) != -1) {
            emit authorCommentDocumentFinished(m_result);
        }

        if (m_result.indexOf("acceptDocument", Qt::CaseInsensitive) != -1) {
            emit acceptDocumentFinished(m_result);
        }

        if (m_result.indexOf("downloadDocument", Qt::CaseInsensitive) != -1) {
            emit downloadDocumentFinished(m_result);
        }

        if (m_result.indexOf("getUsersDocumentStageData", Qt::CaseInsensitive) != -1) {
            emit getUsersDocumentStageDataFinished(m_result);
        }

        if (m_result.indexOf("uploadDocument", Qt::CaseInsensitive) != -1) {
            emit uploadDocumentFinished(m_result);
        }

        if (m_result.indexOf("stageDocumentDownload", Qt::CaseInsensitive) != -1) {
            emit downloadDocumentByStageFinished(m_result);
        }




        if (m_result.indexOf("newEventsNotification", Qt::CaseInsensitive) != -1) {
            emit newEventsNotificationRecieved(m_result);
            m_result.clear();
        }

        if (m_result.indexOf("newDocumentRecieved", Qt::CaseInsensitive) != -1) {
            emit newDocumentRecieved(m_result);
        }

        if (m_result.indexOf("newDocumentAccepted", Qt::CaseInsensitive) != -1) {
            emit newDocumentAccepted(m_result);
        }

        if (m_result.indexOf("newDocumentRejected", Qt::CaseInsensitive) != -1) {
            emit newDocumentRejected(m_result);
        }

        if (m_result.indexOf("newDocumentCommented", Qt::CaseInsensitive) != -1) {
            emit newDocumentCommented(m_result);
        }

        if (m_result.indexOf("authorDocumentRejected", Qt::CaseInsensitive) != -1) {
            emit authorDocumentRejected(m_result);
        }

        if (m_result.indexOf("authorDocumentCommented", Qt::CaseInsensitive) != -1) {
            emit authorDocumentCommented(m_result);
        }

        if (m_result.indexOf("newDocumentUploaded", Qt::CaseInsensitive) != -1) {
            emit newDocumentUploaded(m_result);
        }

        m_result.clear();
    }
}

//Вход пользователя
void TCPClient::userEnter(const QString &userLogin,
                          const QString &userPasswordHash)
{
    m_client->write(setBlockQuerySymbols(QString("userEnter&%1&%2")
                                         .arg(userLogin)
                                         .arg(userPasswordHash))
                    .toLocal8Bit());

}

//Вход пользователя (повторный, при переподключении)
void TCPClient::userReEnter(const QString &userToken)
{
    m_client->write(setBlockQuerySymbols(QString("userReEnter&%1")
                                         .arg(userToken))
                    .toLocal8Bit());
}

//Выход пользователя
void TCPClient::userQuit(const QString &userToken)
{
    m_client->write(setBlockQuerySymbols(QString("userQuit&%1")
                                         .arg(userToken))
                    .toLocal8Bit());
}

//Получить список всех пользователей (кроме себя)
void TCPClient::getUserList(const QString &userToken)
{
    m_client->write(setBlockQuerySymbols(QString("getUserList&%1")
                                         .arg(userToken)).toLocal8Bit());
}

//Создать новый документ
void TCPClient::createNewDocument(const QString &userToken,
                                  const QString &documentName,
                                  const QString &documentShortDescription,
                                  const QString &documentData,
                                  const QString &chooseUsersList,
                                  const QString &documentFormat)
{
    m_client->write(setBlockQuerySymbols(QString("createNewDocument&%1&%2&%3&%4&%5&%6")
                                         .arg(userToken)
                                         .arg(documentName)
                                         .arg(documentShortDescription)
                                         .arg(documentData)
                                         .arg(chooseUsersList)
                                         .arg(documentFormat)).toLocal8Bit());
}

//Получить список моих документов
void TCPClient::getMyDocumentList(const QString &userToken)
{
    m_client->write(setBlockQuerySymbols(QString("getMyDocumentList&%1")
                                         .arg(userToken)).toLocal8Bit());
}

//Получить список моих документов (подпись)
void TCPClient::getMyDocumentSignatureList(const QString &userToken)
{
    m_client->write(setBlockQuerySymbols(QString("getMyDocumentSignatureList&%1")
                                         .arg(userToken)).toLocal8Bit());
}

//Отклонить документ
void TCPClient::rejectDocument(const QString &userToken,
                               const int &documentID,
                               const QString &comment)
{
    m_client->write(setBlockQuerySymbols(QString("rejectDocument&%1&%2&%3")
                                         .arg(userToken)
                                         .arg(documentID)
                                         .arg(comment)).toLocal8Bit());
}

//Отклонить документ (автором)
void TCPClient::authorRejectDocument(const QString &userToken,
                                     const int &documentID,
                                     const QString &comment)
{
    m_client->write(setBlockQuerySymbols(QString("authorRejectDocument&%1&%2&%3")
                                         .arg(userToken)
                                         .arg(documentID)
                                         .arg(comment)).toLocal8Bit());
}

//Получить историю документа
void TCPClient::getDocumentHistory(const int &documentID)
{
    m_client->write(setBlockQuerySymbols(QString("getDocumentHistory&%1")
                                         .arg(documentID)).toLocal8Bit());
}

//Уточнение по документу
void TCPClient::commentDocument(const QString &userToken,
                                const int &documentID,
                                const QString &comment)
{
    m_client->write(setBlockQuerySymbols(QString("commentDocument&%1&%2&%3")
                                         .arg(userToken)
                                         .arg(documentID)
                                         .arg(comment)).toLocal8Bit());
}

//Уточнение по документу (автором)
void TCPClient::authorCommentDocument(const QString &userToken,
                                      const int &documentID,
                                      const QString &comment)
{
    m_client->write(setBlockQuerySymbols(QString("authorCommentDocument&%1&%2&%3")
                                         .arg(userToken)
                                         .arg(documentID)
                                         .arg(comment)).toLocal8Bit());
}

//Принятие документа
void TCPClient::acceptDocument(const QString &userToken,
                               const int &documentID,
                               const QString &comment)
{
    m_client->write(setBlockQuerySymbols(QString("acceptDocument&%1&%2&%3")
                                         .arg(userToken)
                                         .arg(documentID)
                                         .arg(comment)).toLocal8Bit());
}

//Скачивание документа
void TCPClient::downloadDocument(const QString &userToken,
                                 const int &documentID)
{
    m_client->write(setBlockQuerySymbols(QString("downloadDocument&%1&%2")
                                         .arg(userToken)
                                         .arg(documentID)).toLocal8Bit());
}

//Получить список сотрудников со статусом документа по каждому сотруднику
void TCPClient::getUsersDocumentStageData(const QString &userToken,
                                          const int &documentID)
{
    m_client->write(setBlockQuerySymbols(QString("getUsersDocumentStageData&%1&%2")
                                         .arg(userToken)
                                         .arg(documentID)).toLocal8Bit());
}

//Отправка документа
void TCPClient::uploadDocument(const QString &userToken,
                               const int &documentID,
                               const QString &documentData,
                               const QString &documentFormat,
                               const QString &comment)
{
    m_client->write(setBlockQuerySymbols(QString("uploadDocument&%1&%2&%3&%4&%5")
                                         .arg(userToken)
                                         .arg(documentID)
                                         .arg(documentData)
                                         .arg(documentFormat)
                                         .arg(comment)).toLocal8Bit());
}

//Скачать документ (по стадии готовности)
void TCPClient::downloadDocumentByStage(const QString &userToken,
                                        const int &userDocumentUploaderID,
                                        const int &documentID,
                                        const int &documentStageID)
{
    m_client->write(setBlockQuerySymbols(QString("stageDocumentDownload&%1&%2&%3&%4")
                                         .arg(userToken)
                                         .arg(documentID)
                                         .arg(userDocumentUploaderID)
                                         .arg(documentStageID)).toLocal8Bit());
}

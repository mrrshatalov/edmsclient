#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include <QObject>

#include <QTcpSocket>
#include <QTimer>
#include <QByteArray>
#include <QFile>
#include <QDataStream>

#include <QDebug>

class TCPClient : public QObject
{
    Q_OBJECT
public:
    explicit TCPClient(QObject *parent = nullptr);
    ~TCPClient();

    void startTCPClient(const QString &serverAddress,
                        const quint16 &serverPort);

    //Вход пользователя
    void userEnter(const QString &userLogin,
                   const QString &userPasswordHash);

    //Вход пользователя (повторный, при переподключении)
    void userReEnter(const QString &userToken);

    //Выход пользователя
    void userQuit(const QString &userToken);

    //Получить список всех пользователей (кроме себя)
    void getUserList(const QString &userToken);

    //Создать новый документ
    void createNewDocument(const QString &userToken,
                           const QString &documentName,
                           const QString &documentShortDescription,
                           const QString &documentData,
                           const QString &chooseUsersList,
                           const QString &documentFormat);

    //Получить список моих документов (созданных)
    void getMyDocumentList(const QString &userToken);

    //Получить список моих документов (подпись)
    void getMyDocumentSignatureList(const QString &userToken);

    //Отклонить документ
    void rejectDocument(const QString &userToken,
                        const int &documentID,
                        const QString &comment);

    //Отклонить документ (автором)
    void authorRejectDocument(const QString &userToken,
                              const int &documentID,
                              const QString &comment);

    //Получить историю документа
    void getDocumentHistory(const int &documentID);

    //Уточнение по документу
    void commentDocument(const QString &userToken,
                         const int &documentID,
                         const QString &comment);

    //Уточнение по документу (автором)
    void authorCommentDocument(const QString &userToken,
                               const int &documentID,
                               const QString &comment);

    //Принятие документа
    void acceptDocument(const QString &userToken,
                        const int &documentID,
                        const QString &comment);

    //Скачивание документа
    void downloadDocument(const QString &userToken,
                          const int &documentID);

    //Получить список сотрудников со статусом документа по каждому сотруднику
    void getUsersDocumentStageData(const QString &userToken,
                                   const int &documentID);

    //Загрузка документа
    void uploadDocument(const QString &userToken,
                        const int &documentID,
                        const QString &documentData,
                        const QString &documentFormat,
                        const QString &comment);

    //Скачать документ (по стадии готовности)
    void downloadDocumentByStage(const QString &userToken,
                                 const int &userDocumentUploaderID,
                                 const int &documentID,
                                 const int &documentStageID);

signals:
    void clientConnectedToServer(const bool &result);

    void userEntered(const QString &jsonData);
    void userQuited(const QString &jsonData);
    void getUserListFinished(const QString &jsonData);
    void createNewDocumentFinished(const QString &jsonData);
    void getMyDocumentListFinished(const QString &jsonData);
    void getMyDocumentSignatureListFinished(const QString &jsonData);
    void rejectDocumentFinished(const QString &jsonData);
    void authorRejectDocumentFinished(const QString &jsonData);
    void getDocumentHistoryFinished(const QString &jsonData);
    void commentDocumentFinished(const QString &jsonData);
    void authorCommentDocumentFinished(const QString &jsonData);
    void acceptDocumentFinished(const QString &jsonData);
    void downloadDocumentFinished(const QString &jsonData);
    void getUsersDocumentStageDataFinished(const QString &jsonData);
    void uploadDocumentFinished(const QString &jsonData);
    void downloadDocumentByStageFinished(const QString &jsonData);

    void newDocumentRecieved(const QString &jsonData);
    void newDocumentAccepted(const QString &jsonData);
    void newDocumentRejected(const QString &jsonData);
    void authorDocumentRejected(const QString &jsonData);
    void newDocumentCommented(const QString &jsonData);
    void authorDocumentCommented(const QString &jsonData);
    void newDocumentUploaded(const QString &jsonData);
    void newEventsNotificationRecieved(const QString &jsonData);


private slots:
    void onClientStateChanged(QAbstractSocket::SocketState socketState);
    void onClientReadyRead();
    void startClientSlot();

private:
    QTcpSocket *m_client;
    QString m_serverAddress;
    quint16 m_serverPort;

    QString m_result;
    QString setBlockQuerySymbols(QString data); //Установка начала и конце блока запроса
};

#endif // TCPCLIENT_H
